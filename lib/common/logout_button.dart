import 'package:flutter/material.dart';

import '../features/profile/services/profile_service.dart';

class LogOutButton extends StatelessWidget {
  const LogOutButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          boxShadow: [
            BoxShadow(
              blurRadius: 10,
              color: Colors.black38,
              spreadRadius: 1,
              offset: Offset(4, 1),
            ),
          ],
        ),
        child: CircleAvatar(
          child: IconButton(
            onPressed: () => showDialog(
              context: context,
              builder: (context) => AlertDialog(
                content: const Text('Are you sure you want to log out?'),
                actions: [
                  TextButton(
                    child: const Text("CANCEL"),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                  TextButton(
                    child: const Text("LOG OUT"),
                    onPressed: () => ProfileService().logOut(context),
                  ),
                ],
              ),
            ),
            icon: const Icon(Icons.logout_rounded),
          ),
        ),
      ),
    );
  }
}
