import 'package:battleship_application/constants/global_variables.dart';
import 'package:battleship_application/features/game/screens/game_mode_screen.dart';
import 'package:battleship_application/features/leaderboard/screens/leaderboard_screen.dart';
import 'package:battleship_application/features/profile/screens/profile_screen.dart';
import 'package:battleship_application/features/statistics/screens/statistics_screen.dart';
import 'package:flutter/material.dart';

import 'logout_button.dart';

class AppNavigationBar extends StatefulWidget {
  static const String routeName = '/nav-bar';
  const AppNavigationBar({Key? key}) : super(key: key);

  @override
  State<AppNavigationBar> createState() => _AppNavigationBarState();
}

class _AppNavigationBarState extends State<AppNavigationBar> {
  int _page = 2;

  final List<Widget> _pages = [
    const StatisticsScreen(),
    const LeaderboardScreen(),
    const GameModeScreen(),
    const ProfileScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: const [
          LogOutButton(),
        ],
      ),
      body: SafeArea(
        child: _pages[_page],
      ),
      bottomNavigationBar: Container(
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(30),
            topLeft: Radius.circular(30),
          ),
          boxShadow: [
            BoxShadow(
              color: Theme.of(context).shadowColor,
              blurRadius: 7,
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            currentIndex: _page,
            selectedIconTheme: IconThemeData(
              size: 40,
              shadows: [
                Shadow(
                  color: Theme.of(context).shadowColor,
                  blurRadius: 8,
                  offset: const Offset(2, 1),
                ),
              ],
            ),
            unselectedIconTheme: const IconThemeData(
              size: 30,
            ),
            onTap: (value) => setState(() {
              _page = value;
            }),
            items: const [
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.bar_chart_rounded,
                ),
                label: GlobalVariables.statisticsLabel,
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.auto_awesome_rounded,
                ),
                label: GlobalVariables.leaderboardLabel,
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.videogame_asset_rounded,
                ),
                label: GlobalVariables.gameLabel,
              ),
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.person_rounded,
                ),
                label: GlobalVariables.profileLabel,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
