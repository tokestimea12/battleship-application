import 'dart:convert';

import 'package:flutter/material.dart';

class UserData extends StatelessWidget {
  final String profilePicture;
  final String userName;

  const UserData({
    Key? key,
    required this.profilePicture,
    required this.userName,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  blurRadius: 10,
                  color: Theme.of(context).shadowColor,
                  offset: const Offset(0.5, 0.5),
                ),
              ],
            ),
            child: CircleAvatar(
              backgroundImage: MemoryImage(
                base64Decode(profilePicture),
              ),
              radius: 50,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            userName,
            style: Theme.of(context).textTheme.headline6,
          ),
        ],
      ),
    );
  }
}
