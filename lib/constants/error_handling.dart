import 'dart:convert';

import 'package:battleship_application/constants/utils.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void handleHttpError({
  required http.Response response,
  required BuildContext context,
  required VoidCallback onSuccess,
}) {
  switch (response.statusCode) {
    case 200:
      onSuccess();
      break;
    case 400:
      showSnackbar(context, jsonDecode(response.body)['errmsg']);
      break;
    case 500:
      showSnackbar(context, jsonDecode(response.body)['errmsg']);
      break;
    default:
      showSnackbar(context, response.body);
  }
}
