import 'package:flutter/material.dart';

String uri = 'http://10.0.2.2:8000';
//String uri = 'http://192.168.0.220:8000';

class GlobalVariables {
  //colors
  static const seedColor = Color.fromARGB(255, 8, 45, 139);
  static const userCardBackgoundColor = Color.fromARGB(208, 97, 161, 214);
  static const tileBackgroundColor = Color.fromARGB(208, 0, 88, 159);
  static const tileOutlineColor = Color.fromARGB(255, 219, 237, 252);
  static const allowShipColor = Color.fromARGB(200, 105, 168, 107);
  static const rejectShipColor = Color.fromARGB(200, 217, 84, 75);
  static const selectedShipColor = Color.fromARGB(200, 79, 125, 161);
  static const lostGameColor = Color.fromARGB(199, 234, 97, 87);
  static const wonGameColor = Color.fromARGB(199, 75, 156, 217);

  //images
  static const String logo = 'assets/images/logo.png';
  static const String ship2 = 'assets/images/ship.png';
  static const String ship3 = 'assets/images/ship3.png';
  static const String ship4 = 'assets/images/ship4.png';
  static const String ship5 = 'assets/images/ship5.png';
  static const String destroyedShip2 = 'assets/images/ship_destroyed.png';
  static const String destroyedShip3 = 'assets/images/ship3_destroyed.png';
  static const String destroyedShip4 = 'assets/images/ship4_destroyed.png';
  static const String destroyedShip5 = 'assets/images/ship5_destroyed.png';

  //labels
  static const statisticsLabel = 'Statistics';
  static const gameLabel = 'Game';
  static const profileLabel = 'Profile';
  static const leaderboardLabel = 'Podium';
}
