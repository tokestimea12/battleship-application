import 'package:flutter/material.dart';

void showSnackbar(BuildContext context, String text) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(text),
    ),
  );
}

enum Turn {
  player1,
  player2,
}

enum Direction {
  north,
  south,
  east,
  west,
}

enum GameMode {
  random,
  createRoom,
}
