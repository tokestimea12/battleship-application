import 'dart:convert';

import 'package:battleship_application/common/navigation_bar.dart';
import 'package:battleship_application/constants/error_handling.dart';
import 'package:battleship_application/models/user.dart';
import 'package:battleship_application/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../constants/global_variables.dart';
import '../../../constants/utils.dart';

class AuthService {
  void signUp({
    required BuildContext context,
    required String email,
    required String userName,
    required String pswd1,
    required String pswd2,
  }) async {
    try {
      User user = User(
          userId: 0,
          userName: userName,
          email: email,
          pswd1: pswd1,
          pswd2: pswd2,
          token: '',
          profilePicture: '',
          points: 0);

      http.Response response = await http.post(
        Uri.parse('$uri/signup'),
        body: user.toJson(),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () async {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('Welcome!'),
              content: const Text(
                  'Your account has been created! Enjoy the game! :)'),
              actions: [
                TextButton(
                    onPressed: () async {
                      SharedPreferences sharedPreferences =
                          await SharedPreferences.getInstance();

                      Provider.of<UserProvider>(context, listen: false)
                          .setUser(response.body);

                      await sharedPreferences.setString(
                          'token', jsonDecode(response.body)['token']);

                      Navigator.of(context).pushNamedAndRemoveUntil(
                        AppNavigationBar.routeName,
                        (route) => false,
                      );
                    },
                    child: const Text('Let\'s start!'))
              ],
            ),
          );
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  void logIn({
    required BuildContext context,
    required String userName,
    required String pswd,
  }) async {
    try {
      http.Response response = await http.post(
        Uri.parse('$uri/login'),
        body: json.encode({
          'userName': userName,
          'pswd': pswd,
        }),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () async {
          SharedPreferences sharedPreferences =
              await SharedPreferences.getInstance();

          Provider.of<UserProvider>(context, listen: false)
              .setUser(response.body);

          await sharedPreferences.setString(
              'token', jsonDecode(response.body)['token']);

          Navigator.of(context).pushNamedAndRemoveUntil(
            AppNavigationBar.routeName,
            (route) => false,
          );
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  // get user data
  void getUserData(
    BuildContext context,
  ) async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? token = prefs.getString('token');

      if (token == null) {
        prefs.setString('token', '');
        token = '';
      }

      http.Response response = await http.post(
        Uri.parse('$uri/checkToken'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': token
        },
      );

      var decodedResponse = jsonDecode(response.body);

      if (decodedResponse == true) {
        http.Response response2 = await http.get(
          Uri.parse('$uri/'),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'token': token
          },
        );

        var userProvider = Provider.of<UserProvider>(context, listen: false);
        userProvider.setUser(response2.body);
      }
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }
}
