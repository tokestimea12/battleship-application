import 'package:flame/game.dart';

import '../../../providers/game_model.dart';
import '../services/game_service.dart';
import 'ship_user.dart';
import 'tile_user.dart';

class BattleShip extends FlameGame with HasDraggables, HasTappables {
  GameModel gameModel;
  double tileSize = 0;
  final GameService _gameService = GameService();

  BattleShip({
    required this.gameModel,
  });

  @override
  void onGameResize(canvasSize) {
    super.onGameResize(canvasSize);
    tileSize = size.x / gameModel.boardSize;
  }

  @override
  Future<void> onLoad() async {
    super.onLoad();
    if (!gameModel.gameStarted) {
      await addAll(gameModel.boardOpponent.tiles);
      await addAll(gameModel.boardUser.tiles);
      await addAll(gameModel.boardUser.ships);
      await addAll(gameModel.boardUser.goodShotSprites);
      await addAll(gameModel.boardOpponent.ships);
      await addAll(gameModel.boardOpponent.goodShotSprites);
    }
  }

  @override
  void update(double dt) async {
    super.update(dt);

    if (gameModel.gameStarted) {
      _colorTilesBlank();
    }

    if (_allShipsDestroyed()) {
      _gameService.emitGameOver(buildContext!, 'shipsDestroyed');
      pauseEngine();
    }
  }

  void _colorTilesBlank() {
    for (TileUser tile in gameModel.boardUser.tiles) {
      tile.setBlank();
    }
  }

  bool _allShipsDestroyed() {
    for (ShipUser ship in gameModel.boardUser.ships) {
      if (!ship.destroyed) {
        return false;
      }
    }
    return true;
  }
}
