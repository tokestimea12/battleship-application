import 'package:flame/components.dart';
import 'package:flame/extensions.dart';

import '../../../constants/utils.dart';
import 'battle_ship.dart';
import 'tile.dart';

class ShipOpponent extends SpriteComponent with HasGameRef<BattleShip> {
  String imageName;
  int length;
  Direction direction = Direction.north;

  TileOpponent startTile;
  List<TileOpponent> occupiedTiles = [];

  bool selected = false;
  bool destroyed = false;

  ShipOpponent({
    required this.startTile,
    required this.length,
    required this.imageName,
  });

  @override
  Future<void> onLoad() async {
    super.onLoad();

    anchor = Anchor.center;
    size = Vector2(gameRef.tileSize, length * gameRef.tileSize);
    position = Vector2(
        gameRef.tileSize * (startTile.id % gameRef.gameModel.boardSize) +
            size.x / 2,
        gameRef.size.y - gameRef.tileSize * length / 2);

    sprite = await Sprite.load(imageName);
  }

  @override
  void render(Canvas canvas) {
    if (gameRef.gameModel.gameStarted &&
        gameRef.gameModel.turn == Turn.player1 &&
        destroyed &&
        !gameRef.gameModel.gameOver) {
      super.render(canvas);
    }
  }

  void setShipData(double positionX, double positionY, double angle) {
    destroyed = true;
    position.x = positionX;
    position.y = positionY;
    this.angle = angle;
  }
}
