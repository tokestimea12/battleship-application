import 'dart:ui';

import 'package:flame/components.dart';

import '../../../constants/utils.dart';
import 'battle_ship.dart';
import 'tile.dart';
import 'tile_user.dart';

class ShipShotComponent extends SpriteComponent with HasGameRef<BattleShip> {
  TileUser? tileUser;
  TileOpponent? tileOpponent;

  ShipShotComponent({
    this.tileUser,
    this.tileOpponent,
    required tileSize,
    required position,
  }) : super(priority: 1) {
    size = Vector2.all(tileSize);
    this.position = position;
  }

  @override
  Future<void> onLoad() async {
    super.onLoad();

    sprite = await Sprite.load("yes_ship.png");
  }

  @override
  void render(Canvas canvas) {
    if (gameRef.gameModel.gameStarted && !gameRef.gameModel.gameOver) {
      if (gameRef.gameModel.turn == Turn.player2 && tileUser != null) {
        if (tileUser!.tapped && tileUser!.ship != null) {
          super.render(canvas);
          sprite?.render(
            canvas,
            size: size,
          );
        }
      } else if (gameRef.gameModel.turn == Turn.player1 &&
          tileOpponent != null) {
        if (tileOpponent!.tapped && tileOpponent!.isShipHere) {
          super.render(canvas);
          sprite?.render(
            canvas,
            size: size,
          );
        }
      }
    } else if (gameRef.gameModel.gameOver) {
      if (tileUser != null) {
        if (tileUser!.tapped && tileUser!.ship != null) {
          super.render(canvas);
          sprite?.render(
            canvas,
            size: size,
          );
        }
      }
    }
  }
}
