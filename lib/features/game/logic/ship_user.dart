import 'dart:math' as math;

import 'package:battleship_application/features/game/logic/tile_user.dart';
import 'package:flame/components.dart';
import 'package:flame/extensions.dart';
import 'package:flame/input.dart';

import '../../../constants/utils.dart';
import '../services/game_service.dart';
import 'battle_ship.dart';

class ShipUser extends SpriteComponent
    with Tappable, Draggable, HasGameRef<BattleShip> {
  String imageName;
  int length;
  Direction direction = Direction.north;

  TileUser startTile;
  List<TileUser> occupiedTiles = [];

  Vector2 _previousPosition = Vector2.zero();
  List<TileUser> _previousOccupiedTiles = [];
  late TileUser _previousStartTile;

  bool selected = false;
  bool destroyed = false;

  final GameService _gameService = GameService();

  ShipUser({
    required this.startTile,
    required this.length,
    required this.imageName,
  }) {
    _previousStartTile = startTile;
  }

  @override
  Future<void> onLoad() async {
    super.onLoad();

    anchor = Anchor.center;
    size = Vector2(gameRef.tileSize, length * gameRef.tileSize);
    position = Vector2(
        gameRef.tileSize * (startTile.id % gameRef.gameModel.boardSize) +
            size.x / 2,
        gameRef.size.y - gameRef.tileSize * length / 2);

    sprite = await Sprite.load(imageName);

    _setOccupiedTiles();
    _setShipAtTiles();
  }

  @override
  void render(Canvas canvas) {
    if (!gameRef.gameModel.gameStarted ||
        (gameRef.gameModel.gameStarted &&
            gameRef.gameModel.turn == Turn.player2) ||
        gameRef.gameModel.gameOver) {
      super.render(canvas);
    }
  }

  @override
  bool onTapUp(TapUpInfo info) {
    if (gameRef.gameModel.gameStarted) {
      return false;
    }

    _colorTilesBlank();
    if (selected == false) {
      selected = true;

      for (ShipUser ship in gameRef.gameModel.boardUser.ships) {
        if (ship != this) {
          ship.selected = false;
        }
      }

      _colorTilesSelected();

      return true;
    }

    Direction previuosDirection = direction;
    double previousAngle = angle;
    Vector2 previousPosition = Vector2.copy(position);
    TileUser previousStartTile = startTile;
    List<TileUser> previousOccupiedTiles = List.from(occupiedTiles);

    if (direction == Direction.north) {
      angle = math.pi / 2;
      direction = Direction.east;
      if (length % 2 == 0) {
        x = x + gameRef.tileSize / 2;
        y = y + gameRef.tileSize / 2;
      }
    } else if (direction == Direction.south) {
      angle = -math.pi / 2;
      direction = Direction.west;
      if (length % 2 == 0) {
        x = x - gameRef.tileSize / 2;
        y = y - gameRef.tileSize / 2;
      }
    } else if (direction == Direction.west) {
      angle = 0;
      direction = Direction.north;
      if (length % 2 == 0) {
        x = x + gameRef.tileSize / 2;
        y = y - gameRef.tileSize / 2;
      }
    } else {
      angle = math.pi;
      direction = Direction.south;
      if (length % 2 == 0) {
        x = x - gameRef.tileSize / 2;
        y = y + gameRef.tileSize / 2;
      }
    }

    _arrangeShip();
    _setStartTile(toRect());

    if (_overlapsWithOtherShip()) {
      direction = previuosDirection;
      angle = previousAngle;
      position = Vector2.copy(previousPosition);
      startTile = previousStartTile;
      occupiedTiles = List.from(previousOccupiedTiles);
    }

    _colorTilesSelected();
    _setShipAtTiles();

    return true;
  }

  @override
  bool onDragStart(DragStartInfo info) {
    if (gameRef.gameModel.gameStarted) {
      return false;
    }

    _colorTilesBlank();
    selected = true;

    for (ShipUser ship in gameRef.gameModel.boardUser.ships) {
      if (ship != this) {
        selected = false;
      }
    }
    _colorTilesSelected();
    _previousPosition = Vector2.copy(position);
    _previousOccupiedTiles = List.from(occupiedTiles);
    return false;
  }

  @override
  bool onDragUpdate(DragUpdateInfo info) {
    if (gameRef.gameModel.gameStarted) {
      return false;
    }

    if (direction == Direction.north || direction == Direction.south) {
      position = Vector2(
          math.min(
            gameRef.size.x - size.x / 2,
            math.max(size.x / 2, x + info.delta.game.x),
          ),
          math.min(
            gameRef.size.y - size.y / 2,
            math.max(size.y / 2, y + info.delta.game.y),
          ));
    } else {
      position = Vector2(
          math.min(
            gameRef.size.x - size.y / 2,
            math.max(size.y / 2, x + info.delta.game.x),
          ),
          math.min(
            gameRef.size.y - size.x / 2,
            math.max(size.x / 2, y + info.delta.game.y),
          ));
    }

    Vector2 currentPosition = Vector2.copy(position);
    Vector2 middlePosition = Vector2.copy(currentPosition);
    if (length % 2 == 0) {
      if (direction == Direction.north || direction == Direction.south) {
        middlePosition = Vector2(
            currentPosition.x, currentPosition.y - gameRef.tileSize / 2);
      } else {
        middlePosition = Vector2(
            currentPosition.x - gameRef.tileSize / 2, currentPosition.y);
      }
    }
    for (TileUser tile in gameRef.gameModel.boardUser.tiles) {
      if ((tile.position & tile.size).containsPoint(middlePosition)) {
        if (length % 2 != 0) {
          currentPosition = (tile.position & tile.size).center.toVector2();
        } else {
          if (direction == Direction.north || direction == Direction.south) {
            currentPosition =
                (tile.position & tile.size).bottomCenter.toVector2();
          } else {
            currentPosition =
                (tile.position & tile.size).centerRight.toVector2();
          }
        }

        if (direction == Direction.north || direction == Direction.south) {
          _setStartTile(Rect.fromCenter(
            center: currentPosition.toOffset(),
            width: width,
            height: height,
          ));
        } else {
          _setStartTile(Rect.fromCenter(
            center: currentPosition.toOffset(),
            width: height,
            height: width,
          ));
        }

        if (_overlapsWithOtherShip()) {
          _colorTilesReject();
        } else {
          _colorTilesAllow();
          _setShipAtTiles();
        }
        return false;
      }
    }
    return false;
  }

  @override
  bool onDragEnd(DragEndInfo info) {
    if (gameRef.gameModel.gameStarted) {
      return false;
    }

    _colorTilesBlank();
    Vector2 middlePosition = Vector2.copy(position);
    if (length % 2 == 0) {
      if (direction == Direction.north || direction == Direction.south) {
        middlePosition = Vector2(x, y - gameRef.tileSize / 2);
      } else {
        middlePosition = Vector2(x - gameRef.tileSize / 2, y);
      }
    }
    for (TileUser tile in gameRef.gameModel.boardUser.tiles) {
      if ((tile.position & tile.size).containsPoint(middlePosition)) {
        if (length % 2 != 0) {
          position = (tile.position & tile.size).center.toVector2();
        } else {
          if (direction == Direction.north || direction == Direction.south) {
            position = (tile.position & tile.size).bottomCenter.toVector2();
          } else {
            position = (tile.position & tile.size).centerRight.toVector2();
          }
        }
        _setStartTile(toRect());
        if (_overlapsWithOtherShip()) {
          position = Vector2.copy(_previousPosition);
          startTile = _previousStartTile;
          occupiedTiles = List.from(_previousOccupiedTiles);
        }

        selected = true;
        _colorTilesSelected();
        _setShipAtTiles();
        return false;
      }
    }
    return false;
  }

  void _arrangeShip() {
    Offset topLeft = toRect().topLeft;
    Offset bottomRight = toRect().bottomRight;

    Rect gameArea = Rect.fromLTWH(
      0,
      0,
      gameRef.tileSize * gameRef.gameModel.boardSize,
      gameRef.tileSize * gameRef.gameModel.boardSize,
    );

    Offset minPositions = gameArea.topLeft;
    Offset maxPositions = gameArea.bottomRight;

    if (topLeft.dx < minPositions.dx) {
      position = Vector2(position.x + minPositions.dx - topLeft.dx, position.y);
    }

    if (topLeft.dy < minPositions.dy) {
      position = Vector2(position.x, position.y + minPositions.dy - topLeft.dy);
    }

    if (bottomRight.dx > maxPositions.dx) {
      position =
          Vector2(position.x - (bottomRight.dx - maxPositions.dx), position.y);
    }

    if (bottomRight.dy > maxPositions.dy) {
      position =
          Vector2(position.x, position.y - (bottomRight.dy - maxPositions.dy));
    }
  }

  void _setStartTile(Rect rect) {
    Vector2 topLeftPosition = Vector2(rect.left, rect.top);
    for (TileUser tile in gameRef.gameModel.boardUser.tiles) {
      if ((tile.position.x - topLeftPosition.x).abs() < 0.0001 &&
          (tile.position.y - topLeftPosition.y).abs() < 0.0001) {
        startTile = tile;
        _setOccupiedTiles();
        return;
      }
    }
  }

  void _setOccupiedTiles() {
    int tileOffset = 1;
    if (direction == Direction.south || direction == Direction.north) {
      tileOffset = gameRef.gameModel.boardSize;
    }

    int tileNo = startTile.id;
    occupiedTiles = [];

    for (int i = 0; i < length; i++) {
      occupiedTiles.add(gameRef.gameModel.boardUser.tiles
          .where((tile) => tile.id == tileNo)
          .first);
      tileNo += tileOffset;
    }
  }

  bool _overlapsWithOtherShip() {
    for (ShipUser ship in gameRef.gameModel.boardUser.ships) {
      if (ship != this) {
        for (TileUser occupiedTile in occupiedTiles) {
          if (ship.occupiedTiles.contains(occupiedTile)) {
            return true;
          }
        }
      }
    }
    return false;
  }

  void _colorTilesBlank() {
    for (TileUser tile in gameRef.gameModel.boardUser.tiles) {
      tile.setBlank();
    }
  }

  void _colorTilesSelected() {
    for (TileUser tile in gameRef.gameModel.boardUser.tiles) {
      if (occupiedTiles.contains(tile)) {
        tile.setSelected();
      }
    }
  }

  void _colorTilesAllow() {
    for (TileUser tile in gameRef.gameModel.boardUser.tiles) {
      if (occupiedTiles.contains(tile)) {
        tile.allowShip();
      } else if (_previousOccupiedTiles.contains(tile)) {
        tile.setSelected();
      } else {
        tile.setBlank();
      }
    }
  }

  void _setShipAtTiles() {
    for (TileUser tile in gameRef.gameModel.boardUser.tiles) {
      if (!occupiedTiles.contains(tile) && tile.ship == this) {
        tile.ship = null;
      } else if (occupiedTiles.contains(tile)) {
        tile.ship = this;
      }
    }
  }

  void _colorTilesReject() {
    for (TileUser tile in gameRef.gameModel.boardUser.tiles) {
      if (occupiedTiles.contains(tile)) {
        tile.rejectShip();
      } else if (_previousOccupiedTiles.contains(tile)) {
        tile.setSelected();
      } else {
        tile.setBlank();
      }
    }
  }

  void isShipDestroyed() {
    for (TileUser tile in occupiedTiles) {
      if (!tile.tapped) {
        return;
      }
    }
    destroyed = true;
    _gameService.emitShipDestoryed(gameRef.buildContext!, startTile.id, length,
        position.x, position.y, angle);
  }
}
