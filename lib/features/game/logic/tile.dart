import 'package:flame/components.dart';
import 'package:flame/events.dart';
import 'package:flutter/material.dart';

import '../../../constants/global_variables.dart';
import '../../../constants/utils.dart';
import '../services/game_service.dart';
import 'battle_ship.dart';

class TileOpponent extends PositionComponent
    with HasGameRef<BattleShip>, Tappable {
  int id;
  late Rect area;
  Sprite? _noShip;
  Sprite? _yesShip;
  bool tapped = false;
  bool isShipHere = false;
  final GameService _gameService = GameService();

  TileOpponent({
    required this.id,
    required tileSize,
    required position,
  }) {
    size = Vector2.all(tileSize);
    this.position = position;
    area = size.toRect();
  }

  @override
  void onGameResize(Vector2 size) {
    super.onGameResize(size);
    area = this.size.toRect();
  }

  @override
  Future<void> onLoad() async {
    super.onLoad();

    _noShip = await Sprite.load("no_ship.png");
    _yesShip = await Sprite.load("yes_ship.png");
  }

  @override
  void render(Canvas canvas) {
    if (gameRef.gameModel.gameStarted &&
        gameRef.gameModel.turn == Turn.player1 &&
        !gameRef.gameModel.gameOver) {
      super.render(canvas);

      canvas.drawRect(
        area,
        Paint()..color = GlobalVariables.tileBackgroundColor,
      );

      canvas.drawRect(
        area,
        Paint()
          ..strokeWidth = 1
          ..color = GlobalVariables.tileOutlineColor
          ..style = PaintingStyle.stroke,
      );

      if (tapped) {
        if (!isShipHere) {
          _noShip?.render(
            canvas,
            size: size,
          );
        } else {
          _yesShip?.render(
            canvas,
            size: size,
          );
        }
      }
    }
  }

  @override
  bool onTapDown(TapDownInfo info) {
    if (tapped == true ||
        !gameRef.gameModel.gameStarted ||
        gameRef.gameModel.turn == Turn.player2 ||
        gameRef.gameModel.gameOver) {
      return false;
    }
    tapped = true;
    _gameService.emitAttacked(gameRef.buildContext!, id);
    return true;
  }
}
