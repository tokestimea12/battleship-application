import 'dart:ui';

import 'package:battleship_application/constants/global_variables.dart';
import 'package:battleship_application/features/game/logic/ship_user.dart';
import 'package:flame/components.dart';

import '../../../constants/utils.dart';
import 'battle_ship.dart';

class TileUser extends PositionComponent with HasGameRef<BattleShip> {
  int id;
  late Rect area;
  Color _backGroundColor = GlobalVariables.tileBackgroundColor;
  Sprite? _noShip;
  ShipUser? ship;
  bool tapped = false;

  TileUser({
    required this.id,
    required tileSize,
    required position,
  }) {
    size = Vector2.all(tileSize);
    this.position = position;
    area = size.toRect();
  }

  @override
  void onGameResize(Vector2 size) {
    super.onGameResize(size);
    area = this.size.toRect();
  }

  @override
  Future<void> onLoad() async {
    super.onLoad();

    _noShip = await Sprite.load("no_ship.png");
  }

  @override
  void render(Canvas canvas) {
    if (!gameRef.gameModel.gameStarted ||
        (gameRef.gameModel.gameStarted &&
            gameRef.gameModel.turn == Turn.player2) ||
        gameRef.gameModel.gameOver) {
      super.render(canvas);

      canvas.drawRect(
        area,
        Paint()..color = _backGroundColor,
      );

      canvas.drawRect(
        area,
        Paint()
          ..strokeWidth = 1
          ..color = GlobalVariables.tileOutlineColor
          ..style = PaintingStyle.stroke,
      );

      if (tapped) {
        if (ship == null) {
          _noShip?.render(
            canvas,
            size: size,
          );
        }
      }
    }
  }

  void allowShip() => _backGroundColor = GlobalVariables.allowShipColor;

  void rejectShip() => _backGroundColor = GlobalVariables.rejectShipColor;

  void setBlank() => _backGroundColor = GlobalVariables.tileBackgroundColor;

  void setSelected() => _backGroundColor = GlobalVariables.selectedShipColor;
}
