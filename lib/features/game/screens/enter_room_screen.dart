import 'package:battleship_application/features/game/services/game_service.dart';
import 'package:flutter/material.dart';

import '../../../common/my_custom_button.dart';
import '../../../constants/global_variables.dart';

class EnterRoomScreen extends StatefulWidget {
  static const String routeName = 'enter-room-screen';
  const EnterRoomScreen({Key? key}) : super(key: key);

  @override
  State<EnterRoomScreen> createState() => _EnterRoomScreenState();
}

class _EnterRoomScreenState extends State<EnterRoomScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _roomIdController = TextEditingController();
  final GameService _gameService = GameService();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Expanded(child: SizedBox()),
              Image.asset(GlobalVariables.logo),
              const SizedBox(
                height: 50,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        controller: _roomIdController,
                        decoration: const InputDecoration(
                          labelText: 'Room ID',
                          filled: true,
                        ),
                        maxLines: 1,
                      ),
                    ),
                    MyCustomButton(
                      text: 'Enter room',
                      onPressed: () {
                        _gameService.onWaitingForOpponent(context);
                        _gameService.emitRoomEntered(
                          context,
                          _roomIdController.text,
                        );
                        _gameService.onError(context);
                      },
                    ),
                  ],
                ),
              ),
              const Expanded(child: SizedBox()),
              const Expanded(child: SizedBox()),
            ],
          ),
        ),
      ),
    );
  }
}
