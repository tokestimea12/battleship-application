import 'dart:async';

import 'package:battleship_application/common/my_custom_button.dart';
import 'package:battleship_application/constants/global_variables.dart';
import 'package:battleship_application/constants/utils.dart';
import 'package:battleship_application/features/game/services/game_service.dart';
import 'package:battleship_application/features/game/widgets/ship_details.dart';
import 'package:flutter/material.dart';

enum BoardSize {
  eight,
  ten,
  twelwe,
}

class GameChooserScreen extends StatefulWidget {
  static const String routeName = 'game-chooser-screen';
  static GameMode gameMode = GameMode.random;

  const GameChooserScreen({Key? key}) : super(key: key);

  @override
  State<GameChooserScreen> createState() => _RandomPlayerScreenState();
}

class _RandomPlayerScreenState extends State<GameChooserScreen>
    with TickerProviderStateMixin {
  late AnimationController _animationController;
  final GameService _gameService = GameService();
  BoardSize _boardSize = BoardSize.eight;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 200));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _animationController.reset();
    Timer(const Duration(milliseconds: 150), () {
      if (mounted) {
        _animationController.forward();
      }
    });
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(GlobalVariables.logo),
              const SizedBox(
                height: 20,
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Select a board size:',
                  style: Theme.of(context).textTheme.titleLarge,
                  textAlign: TextAlign.left,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              RadioListTile(
                tileColor: _boardSize == BoardSize.eight
                    ? Theme.of(context).secondaryHeaderColor
                    : null,
                shape: const Border(
                  bottom: BorderSide(),
                ),
                title: const Text('8x8'),
                value: BoardSize.eight,
                groupValue: _boardSize,
                onChanged: (BoardSize? value) {
                  setState(() {
                    _boardSize = value!;
                  });
                },
              ),
              if (_boardSize == BoardSize.eight)
                SlideTransition(
                  position: Tween<Offset>(
                    begin: const Offset(1, 0),
                    end: Offset.zero,
                  ).animate(_animationController),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: const [
                        ShipDetails(
                          number: '1 x ',
                          shipImage: GlobalVariables.ship4,
                        ),
                        ShipDetails(
                          number: '3 x ',
                          shipImage: GlobalVariables.ship3,
                        ),
                        ShipDetails(
                          number: '2 x ',
                          shipImage: GlobalVariables.ship2,
                        ),
                      ],
                    ),
                  ),
                ),
              RadioListTile(
                tileColor: _boardSize == BoardSize.ten
                    ? Theme.of(context).secondaryHeaderColor
                    : null,
                shape: const Border(
                  bottom: BorderSide(),
                ),
                title: const Text('10x10'),
                value: BoardSize.ten,
                groupValue: _boardSize,
                onChanged: (BoardSize? value) {
                  setState(() {
                    _boardSize = value!;
                  });
                },
              ),
              if (_boardSize == BoardSize.ten)
                SlideTransition(
                  position: Tween<Offset>(
                    begin: const Offset(1, 0),
                    end: Offset.zero,
                  ).animate(_animationController),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: const [
                        ShipDetails(
                          number: '1 x ',
                          shipImage: GlobalVariables.ship5,
                        ),
                        ShipDetails(
                          number: '1 x ',
                          shipImage: GlobalVariables.ship4,
                        ),
                        ShipDetails(
                          number: '2 x ',
                          shipImage: GlobalVariables.ship3,
                        ),
                        ShipDetails(
                          number: '1 x ',
                          shipImage: GlobalVariables.ship2,
                        ),
                      ],
                    ),
                  ),
                ),
              RadioListTile(
                tileColor: _boardSize == BoardSize.twelwe
                    ? Theme.of(context).secondaryHeaderColor
                    : null,
                shape: const Border(
                  bottom: BorderSide(),
                ),
                title: const Text('12x12'),
                value: BoardSize.twelwe,
                groupValue: _boardSize,
                onChanged: (BoardSize? value) {
                  setState(() {
                    _boardSize = value!;
                  });
                },
              ),
              if (_boardSize == BoardSize.twelwe)
                SlideTransition(
                  position: Tween<Offset>(
                    begin: const Offset(1, 0),
                    end: Offset.zero,
                  ).animate(_animationController),
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(
                      children: const [
                        ShipDetails(
                          number: '1 x ',
                          shipImage: GlobalVariables.ship5,
                        ),
                        ShipDetails(
                          number: '2 x ',
                          shipImage: GlobalVariables.ship4,
                        ),
                        ShipDetails(
                          number: '2 x ',
                          shipImage: GlobalVariables.ship3,
                        ),
                        ShipDetails(
                          number: '2 x ',
                          shipImage: GlobalVariables.ship2,
                        ),
                      ],
                    ),
                  ),
                ),
              Expanded(
                child: Container(),
              ),
              GameChooserScreen.gameMode == GameMode.random
                  ? MyCustomButton(
                      text: 'Start game',
                      onPressed: () {
                        _gameService.onWaitingForOpponent(context);
                        _gameService.emitConnectToGame(
                          context,
                          _boardSize.name,
                        );

                        showDialog(
                          barrierDismissible: false,
                          context: context,
                          builder: (dialogContext) {
                            return Dialog(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  Align(
                                    alignment: Alignment.topRight,
                                    child: IconButton(
                                      icon: const Icon(Icons.close_outlined),
                                      onPressed: () {
                                        Navigator.of(dialogContext).pop();
                                        _gameService
                                            .emitWaitingForOpponentClosed(
                                                context);
                                      },
                                    ),
                                  ),
                                  const CircularProgressIndicator(),
                                  const SizedBox(height: 15),
                                  const Text('Waiting for opponent ...'),
                                  const SizedBox(height: 30),
                                ],
                              ),
                            );
                          },
                        );
                      },
                    )
                  : MyCustomButton(
                      text: 'Create room',
                      onPressed: () {
                        _gameService.onRoomCreated(context);
                        _gameService.onWaitingForOpponent(context);
                        _gameService.emitConnectToGameWithRoom(
                          context,
                          _boardSize.name,
                        );
                      },
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
