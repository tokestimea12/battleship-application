import 'package:battleship_application/constants/utils.dart';
import 'package:battleship_application/features/game/screens/enter_room_screen.dart';
import 'package:flutter/material.dart';

import '../../../common/my_custom_button.dart';
import '../services/game_service.dart';
import 'game_chooser_screen.dart';

class GameModeScreen extends StatefulWidget {
  const GameModeScreen({Key? key}) : super(key: key);

  @override
  State<GameModeScreen> createState() => _GameModeScreenState();
}

class _GameModeScreenState extends State<GameModeScreen> {
  final GameService _gameService = GameService();

  @override
  void initState() {
    super.initState();
    _gameService.connectToServer(context);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            'Select a game mode',
            style: Theme.of(context).textTheme.titleLarge,
          ),
          const SizedBox(height: 20),
          MyCustomButton(
            text: 'Game with a random player',
            onPressed: () {
              GameChooserScreen.gameMode = GameMode.random;
              Navigator.of(context).pushNamed(GameChooserScreen.routeName);
            },
          ),
          MyCustomButton(
            text: 'Create a room',
            onPressed: () {
              GameChooserScreen.gameMode = GameMode.createRoom;
              Navigator.of(context).pushNamed(GameChooserScreen.routeName);
            },
          ),
          MyCustomButton(
            text: 'Enter a room',
            onPressed: () =>
                Navigator.of(context).pushNamed(EnterRoomScreen.routeName),
          ),
        ],
      ),
    );
  }
}
