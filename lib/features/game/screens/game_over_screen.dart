import 'dart:convert';

import 'package:battleship_application/features/game/services/game_data_upload_service.dart';
import 'package:battleship_application/providers/game_statistics_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/my_custom_button.dart';
import '../../../common/navigation_bar.dart';
import '../../../providers/user_provider.dart';

class GameOverScreen extends StatefulWidget {
  static const String routeName = '/game-over-screen';

  const GameOverScreen({Key? key}) : super(key: key);

  @override
  State<GameOverScreen> createState() => _GameOverScreenState();
}

class _GameOverScreenState extends State<GameOverScreen> {
  late GameStatisticsProvider _gameStatisticsProvider;
  late int _timeMinutes;
  late int _timeSeconds;
  late String _userName;
  final GameDataUploadService _gameDataUploadService = GameDataUploadService();

  @override
  void initState() {
    super.initState();
    _gameStatisticsProvider =
        Provider.of<GameStatisticsProvider>(context, listen: false);
    _timeMinutes =
        (_gameStatisticsProvider.gameStatistics.gameTimeInSeconds / 60).floor();
    _timeSeconds =
        _gameStatisticsProvider.gameStatistics.gameTimeInSeconds % 60;

    _userName = Provider.of<UserProvider>(context, listen: false).user.userName;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: Column(
                  children: [
                    _gameStatisticsProvider.gameStatistics.winnerUserName ==
                            _userName
                        ? Text(
                            'VICTORY',
                            style: Theme.of(context).textTheme.headline3,
                          )
                        : Text(
                            'DEFEAT',
                            style: Theme.of(context).textTheme.headline3,
                          ),
                    const Expanded(child: SizedBox()),
                    Text(
                      'GAME STATISTICS',
                      style: Theme.of(context).textTheme.headline4,
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    Table(
                      columnWidths: const {
                        0: FlexColumnWidth(2),
                        1: FlexColumnWidth(1),
                      },
                      border: TableBorder.symmetric(
                          inside: BorderSide(
                              color: Theme.of(context).dividerColor)),
                      defaultVerticalAlignment:
                          TableCellVerticalAlignment.middle,
                      children: [
                        TableRow(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Total game time:',
                                style: Theme.of(context).textTheme.headline5,
                                textAlign: TextAlign.end,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: _timeMinutes != 0
                                  ? Text(
                                      '$_timeMinutes min $_timeSeconds s',
                                      style:
                                          Theme.of(context).textTheme.headline6,
                                      textAlign: TextAlign.end,
                                    )
                                  : Text(
                                      '$_timeSeconds s',
                                      style:
                                          Theme.of(context).textTheme.headline5,
                                      textAlign: TextAlign.end,
                                    ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Rounds:',
                                style: Theme.of(context).textTheme.headline5,
                                textAlign: TextAlign.end,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                '${_gameStatisticsProvider.gameStatistics.numberOfRounds}',
                                style: Theme.of(context).textTheme.headline5,
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        ),
                        TableRow(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Points earned:',
                                style: Theme.of(context).textTheme.headline5,
                                textAlign: TextAlign.end,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                '${_gameStatisticsProvider.gameStatistics.winnerUserName == _userName ? _gameStatisticsProvider.gameStatistics.winnerPointsEarned : _gameStatisticsProvider.gameStatistics.loserPointsEarned}',
                                style: Theme.of(context).textTheme.headline5,
                                textAlign: TextAlign.end,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    const SizedBox(
                      height: 32,
                    ),
                    FutureBuilder(
                      future: _gameDataUploadService.getGamePictures(
                          context: context,
                          gameId: _gameStatisticsProvider.gameId),
                      builder: ((context, snapshot) {
                        if (snapshot.connectionState == ConnectionState.done) {
                          if (snapshot.hasError) {
                            return const Text('We ran into an error');
                          }
                          return Row(
                            children: [
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(
                                      'My fleet',
                                      style:
                                          Theme.of(context).textTheme.headline6,
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    _gameStatisticsProvider.gameStatistics
                                                .winnerUserName ==
                                            _userName
                                        ? Image.memory(
                                            base64Decode(_gameStatisticsProvider
                                                .gameStatistics
                                                .winnerFleetPicture),
                                          )
                                        : Image.memory(
                                            base64Decode(_gameStatisticsProvider
                                                .gameStatistics
                                                .loserFleetPicture),
                                          ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                width: 16,
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    Text(
                                      'Enemy fleet',
                                      style:
                                          Theme.of(context).textTheme.headline6,
                                    ),
                                    const SizedBox(
                                      height: 8,
                                    ),
                                    _gameStatisticsProvider.gameStatistics
                                                .winnerUserName ==
                                            _userName
                                        ? Image.memory(
                                            base64Decode(_gameStatisticsProvider
                                                .gameStatistics
                                                .loserFleetPicture),
                                          )
                                        : Image.memory(
                                            base64Decode(_gameStatisticsProvider
                                                .gameStatistics
                                                .winnerFleetPicture),
                                          ),
                                  ],
                                ),
                              ),
                            ],
                          );
                        }
                        return const CircularProgressIndicator();
                      }),
                    ),
                    const Expanded(child: SizedBox()),
                    const Expanded(child: SizedBox()),
                    const Expanded(child: SizedBox()),
                  ],
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: MyCustomButton(
                  text: 'Back to main page',
                  onPressed: () =>
                      Navigator.of(context).pushNamedAndRemoveUntil(
                    AppNavigationBar.routeName,
                    (route) => false,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
