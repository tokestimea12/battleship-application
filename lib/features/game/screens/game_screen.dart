import 'package:battleship_application/features/game/services/game_service.dart';
import 'package:battleship_application/features/game/widgets/opponents_container.dart';
import 'package:battleship_application/features/game/widgets/sliding_user_data_container.dart';
import 'package:battleship_application/features/game/widgets/timer_button.dart';
import 'package:battleship_application/features/game/widgets/user_data_container.dart';
import 'package:battleship_application/providers/room_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../constants/global_variables.dart';
import '../../../constants/utils.dart';
import '../../../providers/game_model.dart';
import '../widgets/game_view.dart';

class GameScreen extends StatefulWidget {
  static const String routeName = '/game-screen';

  const GameScreen({Key? key}) : super(key: key);

  @override
  State<GameScreen> createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  final GameService _gameService = GameService();

  @override
  Widget build(BuildContext context) {
    int boardSize = Provider.of<RoomProvider>(context).room.boardSize;
    double offSet = 2.4;
    if (boardSize == 10) {
      offSet = 2;
    } else if (boardSize == 8) {
      offSet = 1.6;
    }
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        actions: [
          IconButton(
            icon: const Icon(Icons.close_outlined),
            onPressed: () => showDialog(
              context: context,
              builder: (context) => AlertDialog(
                content: const Text('Are you sure you want to leave the game?'),
                actions: [
                  TextButton(
                    child: const Text("CANCEL"),
                    onPressed: () => Navigator.of(context).pop(),
                  ),
                  TextButton(
                    child: const Text("LEAVE"),
                    onPressed: () {
                      _gameService.emitGameOver(context, 'userLeft');
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      body: Center(
        child: Column(
          children: [
            Image.asset(GlobalVariables.logo),
            const Expanded(
              child: SizedBox(),
            ),
            if (Provider.of<RoomProvider>(context).gameStarted)
              Provider.of<RoomProvider>(context).turnChanged
                  ? const SlidingUserDataContainer()
                  : const UserDataContainer(),
            const Expanded(
              child: SizedBox(),
            ),
            ChangeNotifierProvider(
              create: (context) => GameModel(boardSize: boardSize),
              child: Consumer<GameModel>(
                builder: (context, gameModel, state) {
                  if (!gameModel.gameStarted) {
                    gameModel.initGame(MediaQuery.of(context).size.width /
                        (gameModel.boardSize + offSet));
                  }
                  return Column(
                    children: [
                      !gameModel.gameStarted
                          ? Column(
                              children: const [
                                OpponentsContainer(),
                                SizedBox(height: 5),
                                Text('Please arrange your boats!'),
                              ],
                            )
                          : gameModel.turn == Turn.player1
                              ? const Text('Attack!')
                              : const Text('Waiting for attack ...'),
                      const SizedBox(height: 5),
                      GameView(gameModel),
                      const SizedBox(height: 10),
                      TimerButton(gameModel),
                    ],
                  );
                },
              ),
            ),
            const Expanded(
              child: SizedBox(),
            ),
          ],
        ),
      ),
    );
  }
}
