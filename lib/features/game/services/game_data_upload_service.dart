import 'dart:convert';
import 'dart:typed_data';

import 'package:battleship_application/features/game/services/game_service.dart';
import 'package:battleship_application/models/game_statistics.dart';
import 'package:battleship_application/providers/game_statistics_provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:provider/provider.dart';

import '../../../constants/error_handling.dart';
import '../../../constants/global_variables.dart';
import '../../../constants/utils.dart';
import '../../../providers/room_provider.dart';
import '../../../providers/user_provider.dart';

class GameDataUploadService {
  Future<void> uploadGameStatistics({
    required BuildContext context,
    required String winnerUserName,
    required String loserUserName,
    required int gameTimeInSeconds,
    required int numberOfRounds,
    required int winnerPointsEarned,
    required int loserPointsEarned,
  }) async {
    try {
      GameStatistics gameStatistics = GameStatistics(
          winnerUserName: winnerUserName,
          loserUserName: loserUserName,
          gameTimeInSeconds: gameTimeInSeconds,
          numberOfRounds: numberOfRounds,
          winnerFleetPicture: '',
          loserFleetPicture: '',
          winnerPointsEarned: winnerPointsEarned,
          loserPointsEarned: loserPointsEarned,
          gameDate: DateTime.now());

      http.Response response = await http.post(
        Uri.parse('$uri/api/games'),
        body: gameStatistics.toJson(),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': Provider.of<UserProvider>(context, listen: false).user.token,
        },
      );
      handleHttpError(
        response: response,
        context: context,
        onSuccess: () =>
            Provider.of<GameStatisticsProvider>(context, listen: false).gameId =
                jsonDecode(response.body)['gameId'],
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  void getGameStatistics({
    required BuildContext context,
    required int gameId,
  }) async {
    try {
      http.Response response = await http.get(
        Uri.parse('$uri/api/games/$gameId'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': Provider.of<UserProvider>(context, listen: false).user.token,
        },
      );

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () {
          Provider.of<GameStatisticsProvider>(context, listen: false)
              .setGameStatistics(response.body);
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  void uploadFleetPicture({
    required BuildContext context,
    required Uint8List pictureData,
    required String pictureName,
    required int gameId,
    required String userName,
  }) async {
    try {
      http.MultipartRequest request = http.MultipartRequest(
        'PATCH',
        Uri.parse('$uri/api/games/$gameId/$userName'),
      );

      Map<String, String> headers = {
        "Content-type": "multipart/form-data",
        'token': Provider.of<UserProvider>(context, listen: false).user.token,
      };

      request.headers.addAll(headers);
      request.files.add(
        http.MultipartFile.fromBytes(
          'picture',
          pictureData,
          filename: pictureName,
          contentType: MediaType('image', 'png'),
        ),
      );

      var responseStream = await request.send();
      http.Response response = await http.Response.fromStream(responseStream);

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () => GameService.socket!.emit('fleetPictureUploaded', {
          'roomId':
              Provider.of<RoomProvider>(context, listen: false).room.roomId,
        }),
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  Future<void> getGamePictures({
    required BuildContext context,
    required int gameId,
  }) async {
    try {
      http.Response response = await http.get(
        Uri.parse('$uri/api/games/$gameId/pictures'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': Provider.of<UserProvider>(context, listen: false).user.token,
        },
      );

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () {
          Provider.of<GameStatisticsProvider>(context, listen: false)
              .setWinnerFleetPicture(
                  jsonDecode(response.body)['winnerPicture']);

          Provider.of<GameStatisticsProvider>(context, listen: false)
              .setLoserFleetPicture(jsonDecode(response.body)['loserPicture']);
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  void updatePointsForUser({
    required BuildContext context,
    required int points,
  }) async {
    try {
      String userName =
          Provider.of<UserProvider>(context, listen: false).user.userName;
      http.Response response = await http.patch(
        Uri.parse('$uri/api/users/$userName/updatePoints'),
        body: jsonEncode({
          'points': points,
        }),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': Provider.of<UserProvider>(context, listen: false).user.token,
        },
      );
      handleHttpError(
        response: response,
        context: context,
        onSuccess: () =>
            Provider.of<UserProvider>(context, listen: false).incPoints(points),
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }
}
