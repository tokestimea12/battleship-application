import 'dart:async';
import 'dart:convert';

import 'package:battleship_application/features/game/services/game_data_upload_service.dart';
import 'package:battleship_application/providers/game_statistics_provider.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:socket_io_client/socket_io_client.dart' as io;

import '../../../common/user_data.dart';
import '../../../constants/error_handling.dart';
import '../../../constants/global_variables.dart';
import '../../../constants/utils.dart';
import '../../../providers/game_model.dart';
import '../../../providers/room_provider.dart';
import '../../../providers/user_provider.dart';
import '../screens/game_over_screen.dart';
import '../screens/game_screen.dart';
import '../widgets/game_view.dart';
import '../widgets/remaining_time_widget.dart';

class GameService {
  static io.Socket? socket;
  bool _isDialogShowing = false;
  final GameDataUploadService _gameDataUploadService = GameDataUploadService();

  void connectToServer(BuildContext context) {
    socket = io.io(uri, <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
      'auth': {
        'token': Provider.of<UserProvider>(context, listen: false).user.token,
      },
    });
    socket!.connect();
  }

  void emitConnectToGame(BuildContext context, String boardSize) {
    socket!.emit('connectToGame', {
      'userName':
          Provider.of<UserProvider>(context, listen: false).user.userName,
      'boardSize': boardSize,
    });
  }

  void emitConnectToGameWithRoom(BuildContext context, String boardSize) {
    socket!.emit('createRoom', {
      'userName':
          Provider.of<UserProvider>(context, listen: false).user.userName,
      'boardSize': boardSize,
    });
  }

  void onRoomCreated(BuildContext context) {
    socket!.off('roomCreated');
    socket!.on(
        'roomCreated',
        (roomId) => {
              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (dialogContext) {
                  return Dialog(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Align(
                          alignment: Alignment.topRight,
                          child: IconButton(
                            icon: const Icon(Icons.close_outlined),
                            onPressed: () {
                              Navigator.of(dialogContext).pop();
                              emitWaitingForOpponentClosed(context);
                            },
                          ),
                        ),
                        const Text('Your room ID is:'),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: FittedBox(
                            child: Text(
                              roomId,
                              style: Theme.of(context).textTheme.headline6,
                            ),
                          ),
                        ),
                        const CircularProgressIndicator(),
                        const SizedBox(height: 15),
                        const Text('Waiting for opponent ...'),
                        const SizedBox(height: 30),
                      ],
                    ),
                  );
                },
              ),
            });
  }

  void emitRoomEntered(BuildContext context, String roomId) {
    socket!.emit('roomEntered', {
      'userName':
          Provider.of<UserProvider>(context, listen: false).user.userName,
      'roomId': roomId,
    });
  }

  void emitWaitingForOpponentClosed(BuildContext context) {
    socket!.emit('userDoneWaiting', {
      'userName':
          Provider.of<UserProvider>(context, listen: false).user.userName,
    });
  }

  void onWaitingForOpponent(BuildContext context) {
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    RoomProvider roomProvider =
        Provider.of<RoomProvider>(context, listen: false);

    socket!.off('opponentJoined');
    socket!.on('opponentJoined', (room) {
      int boardSize = 12;
      if (room['boardSize'] == 'eight') {
        boardSize = 8;
      } else if (room['boardSize'] == 'ten') {
        boardSize = 10;
      }
      if (room['player1'] == userProvider.user.userName) {
        roomProvider.setRoom(
          room['roomId'],
          room['player1'],
          room['player2'],
          boardSize,
          room['player1Points'],
          room['player2Points'],
        );

        _getPictures(
          context: context,
          roomProvider: roomProvider,
          userProvider: userProvider,
          userName1: room['player1'],
          userName2: room['player2'],
        );
      } else {
        roomProvider.setRoom(
          room['roomId'],
          room['player2'],
          room['player1'],
          boardSize,
          room['player2Points'],
          room['player1Points'],
        );

        _getPictures(
          context: context,
          roomProvider: roomProvider,
          userProvider: userProvider,
          userName1: room['player2'],
          userName2: room['player1'],
        );
      }
    });
  }

  void onError(BuildContext context) {
    socket!.off('error');
    socket!.on('error', (error) => showSnackbar(context, error));
  }

  void _getPictures({
    required BuildContext context,
    required RoomProvider roomProvider,
    required UserProvider userProvider,
    required String userName1,
    required String userName2,
  }) async {
    try {
      http.Response response = await http.get(
        Uri.parse('$uri/api/users/pictures').replace(queryParameters: {
          'userName1': userName1,
          'userName2': userName2,
        }),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': userProvider.user.token,
        },
      );
      handleHttpError(
        response: response,
        context: context,
        onSuccess: () async {
          roomProvider
              .setProfilePicture1(jsonDecode(response.body)['picture1']);
          roomProvider
              .setProfilePicture2(jsonDecode(response.body)['picture2']);

          Navigator.of(context).pop();

          showDialog(
            barrierDismissible: false,
            context: context,
            builder: (dialogContext) {
              Future.delayed(const Duration(seconds: 1), () {
                Navigator.of(dialogContext)
                    .popAndPushNamed(GameScreen.routeName);
              });
              return Dialog(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      const Text('We found you an opponent!'),
                      const SizedBox(
                        height: 10,
                      ),
                      UserData(
                        profilePicture: roomProvider.room.player2Picture,
                        userName: roomProvider.room.player2,
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  void emitGameCanStart(BuildContext context) {
    socket!.emit('gameCanStart', {
      'userName':
          Provider.of<UserProvider>(context, listen: false).user.userName,
      'roomId': Provider.of<RoomProvider>(context, listen: false).room.roomId,
    });
  }

  void onWaitingForOpponentToStart(BuildContext context) {
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    RoomProvider roomProvider =
        Provider.of<RoomProvider>(context, listen: false);
    socket!.off('waitingForOpponentToStart');
    socket!.on('waitingForOpponentToStart', (userWaiting) {
      if (userWaiting == userProvider.user.userName) {
        String otherUser;
        if (userWaiting == roomProvider.room.player1) {
          otherUser = roomProvider.room.player2;
        } else {
          otherUser = roomProvider.room.player1;
        }
        _isDialogShowing = true;
        showDialog(
          barrierDismissible: false,
          context: context,
          builder: (_) {
            return Dialog(
              child: Padding(
                padding: const EdgeInsets.all(20.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    const CircularProgressIndicator(),
                    const SizedBox(height: 15),
                    Text(
                      'Waiting for $otherUser to arrange their boats ...',
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              ),
            );
          },
        );
      }
    });
  }

  void onStartingGame(BuildContext context) {
    socket!.off('startingGame');
    socket!.on('startingGame', (firstUser) async {
      RemainingTimeWidget.remainingTime = 60;
      Provider.of<GameModel>(context, listen: false).gameStarted = true;
      Provider.of<RoomProvider>(context, listen: false).gameStarted = true;
      if (firstUser ==
          Provider.of<UserProvider>(context, listen: false).user.userName) {
        Provider.of<GameModel>(context, listen: false).changeTurn();
        Provider.of<RoomProvider>(context, listen: false).changeTurn();
      }
      if (_isDialogShowing) {
        Navigator.of(context).pop();
        _isDialogShowing = false;
      }

      showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext dialogContext) {
          Future.delayed(const Duration(seconds: 1),
              () => Navigator.of(dialogContext).pop());
          return Dialog(
            child: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const [
                  CircularProgressIndicator(),
                  SizedBox(height: 15),
                  Text(
                    'Game is about to start!',
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          );
        },
      );
    });
  }

  void emitChangeTurn(BuildContext context) {
    socket!.emit('changeTurn', {
      'roomId': Provider.of<RoomProvider>(context, listen: false).room.roomId,
    });
  }

  void onChangeTurn(BuildContext context) {
    socket!.off('changeHasToTurn');
    socket!.on(
      'changeHasToTurn',
      (data) {
        Provider.of<GameModel>(context, listen: false).changeTurn();
        Provider.of<RoomProvider>(context, listen: false).changeTurn();
        Provider.of<RoomProvider>(context, listen: false).incRound();
        RemainingTimeWidget.remainingTime = 60;
      },
    );
  }

  void emitAttacked(BuildContext context, int tileId) {
    socket!.emit('attacked', {
      'roomId': Provider.of<RoomProvider>(context, listen: false).room.roomId,
      'tileId': tileId,
    });
  }

  void emitGoodShot(BuildContext context, int tileId) {
    socket!.emit('goodShot', {
      'roomId': Provider.of<RoomProvider>(context, listen: false).room.roomId,
      'tileId': tileId,
    });
  }

  void emitShipDestoryed(BuildContext context, int tileId, int length,
      double positionX, double positionY, double angle) {
    Provider.of<RoomProvider>(context, listen: false)
        .setUserShipAsDestroyed(length);
    socket!.emit('shipDestroyed', {
      'roomId': Provider.of<RoomProvider>(context, listen: false).room.roomId,
      'tileId': tileId,
      'length': length,
      'positionX': positionX,
      'positionY': positionY,
      'angle': angle,
    });
  }

  void onAttacked(BuildContext context) {
    socket!.off('attackReceived');
    socket!.on(
      'attackReceived',
      (tileId) {
        if (Provider.of<GameModel>(context, listen: false).turn ==
            Turn.player2) {
          if (Provider.of<GameModel>(context, listen: false)
              .changeTappedTileForUser(tileId)) {
            emitGoodShot(context, tileId);
            Provider.of<RoomProvider>(context, listen: false)
                .setTurnChangedToFalse();
          } else {
            emitChangeTurn(context);
          }
        }
      },
    );
  }

  void onGoodShot(BuildContext context) {
    socket!.off('goodShotReceived');
    socket!.on(
      'goodShotReceived',
      (tileId) {
        RemainingTimeWidget.remainingTime = 60;
        if (Provider.of<GameModel>(context, listen: false).turn ==
            Turn.player1) {
          Provider.of<GameModel>(context, listen: false)
              .setShipIsHereForOpponentTile(tileId);
          Provider.of<RoomProvider>(context, listen: false)
              .setTurnChangedToFalse();
        }
      },
    );
  }

  void onShipDestroyedReceived(BuildContext context) {
    socket!.off('shipDestroyedReceived');
    socket!.on(
      'shipDestroyedReceived',
      (shipData) {
        if (Provider.of<GameModel>(context, listen: false).turn ==
            Turn.player1) {
          Provider.of<GameModel>(context, listen: false)
              .boardOpponent
              .setShipAsDestroyed(
                shipData['length'],
                shipData['positionX'].toDouble(),
                shipData['positionY'].toDouble(),
                shipData['angle'].toDouble(),
              );
          Provider.of<RoomProvider>(context, listen: false)
              .setOpponentShipAsDestroyed(shipData['length']);
        }
      },
    );
  }

  void emitGameOver(BuildContext context, String cause) async {
    RoomProvider roomProvider = Provider.of<RoomProvider>(
      context,
      listen: false,
    );
    GameStatisticsProvider gameStatisticsProvider =
        Provider.of<GameStatisticsProvider>(context, listen: false);
    await _gameDataUploadService.uploadGameStatistics(
      context: context,
      winnerUserName: roomProvider.room.player2,
      loserUserName: roomProvider.room.player1,
      gameTimeInSeconds: roomProvider.room.gameTime,
      numberOfRounds: roomProvider.room.round,
      winnerPointsEarned: 100 - roomProvider.room.round,
      loserPointsEarned: cause == 'shipsDestroyed'
          ? ((100 - roomProvider.room.round) / 3).ceil()
          : roomProvider.room.player1Points < 50
              ? -roomProvider.room.player1Points
              : -50,
    );

    socket!.emit('gameOver', {
      'roomId': roomProvider.room.roomId,
      'gameId': gameStatisticsProvider.gameId,
      'cause': cause,
    });
  }

  void emitPictureUploaded(BuildContext context) {
    socket!.emit('fleetPictureUploaded', {
      'roomId': Provider.of<RoomProvider>(context, listen: false).room.roomId,
    });
  }

  void onBothPicturesUploaded(BuildContext context) {
    socket!.off('bothPicturesUploaded');
    socket!.on('bothPicturesUploaded', (gameId) {
      Navigator.of(context).popAndPushNamed(GameOverScreen.routeName);
    });
  }

  void onGameOver(BuildContext context) {
    GameStatisticsProvider gameStatisticsProvider =
        Provider.of<GameStatisticsProvider>(
      context,
      listen: false,
    );
    socket!.off('gameOver');
    socket!.on(
      'gameOver',
      (data) async {
        onBothPicturesUploaded(context);
        _gameDataUploadService.getGameStatistics(
          context: context,
          gameId: data['gameId'],
        );
        gameStatisticsProvider.gameId = data['gameId'];
        Provider.of<GameModel>(context, listen: false).gameOver = true;
        String userName =
            Provider.of<UserProvider>(context, listen: false).user.userName;
        Timer(const Duration(milliseconds: 100), () async {
          if (gameStatisticsProvider.gameStatistics.winnerUserName ==
              userName) {
            _gameDataUploadService.updatePointsForUser(
                context: context,
                points:
                    gameStatisticsProvider.gameStatistics.winnerPointsEarned);
            if (data['cause'] == 'userLeft') {
              showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext dialogContext) {
                  Future.delayed(const Duration(seconds: 2), () async {
                    _gameDataUploadService.uploadFleetPicture(
                        context: context,
                        pictureData: await GameView.screenShot(),
                        pictureName: 'winnerFleetPicture',
                        gameId: data['gameId'],
                        userName: userName);
                  });
                  return Dialog(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: const [
                          Text(
                            'Your opponent left the game. You won!',
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                    ),
                  );
                },
              );
            } else {
              _gameDataUploadService.uploadFleetPicture(
                  context: context,
                  pictureData: await GameView.screenShot(),
                  pictureName: 'winnerFleetPicture',
                  gameId: data['gameId'],
                  userName: userName);
            }
          } else {
            _gameDataUploadService.updatePointsForUser(
                context: context,
                points:
                    gameStatisticsProvider.gameStatistics.loserPointsEarned);
            _gameDataUploadService.uploadFleetPicture(
              context: context,
              pictureData: await GameView.screenShot(),
              pictureName: 'loserFleetPicture',
              gameId: data['gameId'],
              userName: userName,
            );
          }
        });
      },
    );
  }
}
