import 'dart:async';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flame/game.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';

import '../../../providers/game_model.dart';
import '../../../providers/room_provider.dart';
import '../logic/battle_ship.dart';

class GameView extends StatefulWidget {
  final GameModel gameModel;
  static Function screenShot = () {};

  const GameView(this.gameModel, {Key? key}) : super(key: key);

  @override
  State<GameView> createState() => _GameViewState();
}

class _GameViewState extends State<GameView> with TickerProviderStateMixin {
  late final BattleShip game = BattleShip(gameModel: widget.gameModel);
  late AnimationController _animationController;
  static GlobalKey globalKey = GlobalKey();

  @override
  void initState() {
    super.initState();
    GameView.screenShot = screenShot;
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _animationController.reset();
    Timer(const Duration(milliseconds: 100), () {
      if (mounted) {
        _animationController.forward();
      }
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  Future<Uint8List> screenShot() async {
    RenderRepaintBoundary boundary =
        globalKey.currentContext!.findRenderObject() as RenderRepaintBoundary;
    if (boundary.debugNeedsPaint) {
      await Future.delayed(const Duration(milliseconds: 20));
      return screenShot();
    }
    var image = await boundary.toImage();
    ByteData byteData = (await image.toByteData(format: ImageByteFormat.png))!;
    return byteData.buffer.asUint8List();
  }

  @override
  Widget build(BuildContext context) {
    return Provider.of<RoomProvider>(context).turnChanged
        ? FadeTransition(
            opacity: _animationController,
            child: RepaintBoundary(
              key: globalKey,
              child: SizedBox(
                width: MediaQuery.of(context).size.width -
                    MediaQuery.of(context).size.width / 6,
                height: MediaQuery.of(context).size.width -
                    MediaQuery.of(context).size.width / 6,
                child: GameWidget(
                  game: game,
                ),
              ),
            ),
          )
        : RepaintBoundary(
            key: globalKey,
            child: SizedBox(
              width: MediaQuery.of(context).size.width -
                  MediaQuery.of(context).size.width / 6,
              height: MediaQuery.of(context).size.width -
                  MediaQuery.of(context).size.width / 6,
              child: GameWidget(
                game: game,
              ),
            ),
          );
  }
}
