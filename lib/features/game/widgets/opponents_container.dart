import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/user_data.dart';
import '../../../constants/global_variables.dart';
import '../../../providers/room_provider.dart';

class OpponentsContainer extends StatefulWidget {
  const OpponentsContainer({Key? key}) : super(key: key);

  @override
  State<OpponentsContainer> createState() => _OpponentsContainerState();
}

class _OpponentsContainerState extends State<OpponentsContainer>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 200));
    Timer(const Duration(milliseconds: 100), () {
      if (mounted) {
        _animationController.forward();
      }
    });

    super.initState();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(1, 0),
        end: Offset.zero,
      ).animate(_animationController),
      child: Container(
        decoration: BoxDecoration(
          color: GlobalVariables.userCardBackgoundColor,
          border: Border.symmetric(
            horizontal: BorderSide(color: Theme.of(context).dividerColor),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              UserData(
                profilePicture:
                    Provider.of<RoomProvider>(context).room.player1Picture,
                userName: Provider.of<RoomProvider>(context).room.player1,
              ),
              Expanded(
                child: SizedBox(
                  child: Text(
                    'VS.',
                    style: Theme.of(context).textTheme.titleLarge,
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
              UserData(
                profilePicture:
                    Provider.of<RoomProvider>(context).room.player2Picture,
                userName: Provider.of<RoomProvider>(context).room.player2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
