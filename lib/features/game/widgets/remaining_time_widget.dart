import 'dart:async';

import 'package:battleship_application/constants/utils.dart';
import 'package:battleship_application/features/game/services/game_service.dart';
import 'package:battleship_application/providers/room_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RemainingTimeWidget extends StatefulWidget {
  static int remainingTime = 60;

  const RemainingTimeWidget({
    Key? key,
  }) : super(key: key);

  @override
  State<RemainingTimeWidget> createState() => _RemainingTimeWidgetState();
}

class _RemainingTimeWidgetState extends State<RemainingTimeWidget> {
  Timer? _timer;
  final GameService _gameService = GameService();

  void startTimer() async {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) async {
        if (RemainingTimeWidget.remainingTime == 0) {
          setState(() {
            timer.cancel();
            if (Provider.of<RoomProvider>(context, listen: false).room.turn ==
                Turn.player1) {
              _gameService.emitGameOver(context, 'timeUp');
            }
          });
        } else {
          setState(() {
            RemainingTimeWidget.remainingTime--;
          });
          Provider.of<RoomProvider>(context, listen: false).incGameTime();
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    startTimer();
  }

  @override
  void dispose() {
    _timer!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Text('Remaining time: ${RemainingTimeWidget.remainingTime}');
  }
}
