import 'package:flutter/material.dart';

class ShipDetails extends StatelessWidget {
  final String number;
  final String shipImage;

  const ShipDetails({
    Key? key,
    required this.number,
    required this.shipImage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: [
          Text(
            number,
            style: Theme.of(context).textTheme.titleMedium,
          ),
          RotatedBox(
            quarterTurns: 1,
            child: Image.asset(shipImage),
          ),
        ],
      ),
    );
  }
}
