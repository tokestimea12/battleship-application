import 'package:battleship_application/constants/global_variables.dart';
import 'package:battleship_application/models/ship.dart';
import 'package:flutter/material.dart';

class ShipsDetails extends StatefulWidget {
  final List<Ship> shipList;

  const ShipsDetails({Key? key, required this.shipList}) : super(key: key);

  @override
  State<ShipsDetails> createState() => _ShipsDetailsState();
}

class _ShipsDetailsState extends State<ShipsDetails> {
  @override
  Widget build(BuildContext context) {
    List<Widget> ships = [];
    for (Ship ship in widget.shipList) {
      if (ship.isDestroyed) {
        switch (ship.length) {
          case 2:
            ships.add(
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(GlobalVariables.destroyedShip2),
              ),
            );
            break;
          case 3:
            ships.add(
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(GlobalVariables.destroyedShip3),
              ),
            );
            break;
          case 4:
            ships.add(
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(GlobalVariables.destroyedShip4),
              ),
            );
            break;
          case 5:
            ships.add(
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(GlobalVariables.destroyedShip5),
              ),
            );
            break;
        }
      } else {
        switch (ship.length) {
          case 2:
            ships.add(
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(GlobalVariables.ship2),
              ),
            );
            break;
          case 3:
            ships.add(
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(GlobalVariables.ship3),
              ),
            );
            break;
          case 4:
            ships.add(
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(GlobalVariables.ship4),
              ),
            );
            break;
          case 5:
            ships.add(
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.asset(GlobalVariables.ship5),
              ),
            );
            break;
        }
      }
    }

    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        border: Border.all(color: Theme.of(context).dividerColor, width: 2.0),
        borderRadius: const BorderRadius.all(
          Radius.circular(5.0),
        ),
      ),
      child: FittedBox(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: ships,
          ),
        ),
      ),
    );
  }
}
