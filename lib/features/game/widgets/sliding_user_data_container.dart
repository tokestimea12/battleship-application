import 'dart:async';

import 'package:battleship_application/features/game/widgets/remaining_time_widget.dart';
import 'package:battleship_application/features/game/widgets/ships_details.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/user_data.dart';
import '../../../constants/global_variables.dart';
import '../../../constants/utils.dart';
import '../../../models/room.dart';
import '../../../providers/room_provider.dart';

class SlidingUserDataContainer extends StatefulWidget {
  const SlidingUserDataContainer({
    Key? key,
  }) : super(key: key);

  @override
  State<SlidingUserDataContainer> createState() =>
      _SlidingUserDataWidgetState();
}

class _SlidingUserDataWidgetState extends State<SlidingUserDataContainer>
    with TickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _animationController.reset();
    Timer(const Duration(milliseconds: 100), () {
      if (mounted) {
        _animationController.forward();
      }
    });
    Room room = Provider.of<RoomProvider>(context).room;
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(1, 0),
        end: Offset.zero,
      ).animate(_animationController),
      child: Container(
        decoration: BoxDecoration(
          color: GlobalVariables.userCardBackgoundColor,
          border: Border.symmetric(
            horizontal: BorderSide(color: Theme.of(context).dividerColor),
          ),
        ),
        child: room.turn == Turn.player1
            ? Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            right: BorderSide(
                                width: 2.0,
                                color: Theme.of(context).dividerColor),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 8.0),
                          child: Column(
                            children: [
                              UserData(
                                profilePicture: room.player1Picture,
                                userName: room.player1,
                              ),
                              Text('Points: ${room.player1Points}'),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Column(
                          children: [
                            const RemainingTimeWidget(),
                            const SizedBox(height: 8),
                            Text('Round: ${room.round}'),
                            const SizedBox(height: 16),
                            ShipsDetails(shipList: room.player2Ships),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            : Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        decoration: BoxDecoration(
                          border: Border(
                            right: BorderSide(
                                width: 2.0,
                                color: Theme.of(context).dividerColor),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              UserData(
                                profilePicture: room.player2Picture,
                                userName: room.player2,
                              ),
                              Text('Points: ${room.player2Points}'),
                            ],
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0),
                        child: Column(
                          children: [
                            const RemainingTimeWidget(),
                            const SizedBox(height: 8),
                            Text('Round: ${room.round}'),
                            const SizedBox(height: 16),
                            ShipsDetails(shipList: room.player1Ships),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
      ),
    );
  }
}
