import 'dart:async';

import 'package:flutter/material.dart';

import '../../../providers/game_model.dart';
import '../services/game_service.dart';

class TimerButton extends StatefulWidget {
  final GameModel gameModel;

  const TimerButton(this.gameModel, {Key? key}) : super(key: key);

  @override
  State<TimerButton> createState() => _TimerButtonState();
}

class _TimerButtonState extends State<TimerButton> {
  final GameService _gameService = GameService();
  Timer? _timer;
  int _start = 60;

  void startTimer() {
    const oneSec = Duration(seconds: 1);
    _timer = Timer.periodic(
      oneSec,
      (Timer timer) {
        if (_start == 0) {
          setState(() {
            timer.cancel();
          });
          _gameService.emitGameCanStart(context);
          _gameService.onWaitingForOpponentToStart(context);
          _gameService.onStartingGame(context);
          _gameService.onChangeTurn(context);
          _gameService.onAttacked(context);
          _gameService.onGoodShot(context);
          _gameService.onShipDestroyedReceived(context);
          _gameService.onGameOver(context);
        } else {
          setState(() {
            _start--;
          });
        }
      },
    );
  }

  @override
  void initState() {
    super.initState();
    startTimer();
  }

  @override
  void dispose() {
    _timer!.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Visibility(
      visible: widget.gameModel.gameStarted ? false : true,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: ElevatedButton(
          onPressed: () {
            setState(() {
              _timer!.cancel();
            });
            _gameService.emitGameCanStart(context);
            _gameService.onWaitingForOpponentToStart(context);
            _gameService.onStartingGame(context);
            _gameService.onChangeTurn(context);
            _gameService.onAttacked(context);
            _gameService.onGoodShot(context);
            _gameService.onShipDestroyedReceived(context);
            _gameService.onGameOver(context);
          },
          child: Text(
            "Start game\n$_start",
            textAlign: TextAlign.center,
          ),
          style: ElevatedButton.styleFrom(
            minimumSize: const Size.fromHeight(50),
          ),
        ),
      ),
    );
  }
}
