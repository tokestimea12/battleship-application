import 'dart:convert';

import 'package:battleship_application/models/user.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../../../constants/error_handling.dart';
import '../../../constants/global_variables.dart';
import '../../../constants/utils.dart';
import '../../../providers/user_provider.dart';

class LeaderboardService {
  Future<void> getLeaderboard({
    required BuildContext context,
  }) async {
    try {
      http.Response response = await http.get(
        Uri.parse('$uri/api/users/leaderboard'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': Provider.of<UserProvider>(context, listen: false).user.token,
        },
      );

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () {
          Provider.of<UserProvider>(context, listen: false).leaderboard = [];
          for (var user in jsonDecode(response.body)['leaderboard']) {
            Provider.of<UserProvider>(context, listen: false)
                .leaderboard
                .add(User.fromMap(user));
          }
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }
}
