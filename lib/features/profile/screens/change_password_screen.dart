import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/my_custom_button.dart';
import '../../../providers/user_provider.dart';
import '../services/profile_service.dart';
import '../widgets/profile_picture.dart';
import '../widgets/user_name.dart';

class ChangePasswordScreen extends StatefulWidget {
  static const String routeName = '/change-password-screen';

  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  State<ChangePasswordScreen> createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _newPasswordController1 = TextEditingController();
  final TextEditingController _newPasswordController2 = TextEditingController();
  final TextEditingController _oldPasswordController = TextEditingController();

  final ProfileService profileService = ProfileService();

  void changePassword() {
    profileService.changePassword(
      context: context,
      userName: Provider.of<UserProvider>(context, listen: false).user.userName,
      oldPassword: _oldPasswordController.text,
      newPassword1: _newPasswordController1.text,
      newPassword2: _newPasswordController2.text,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _newPasswordController1.dispose();
    _newPasswordController2.dispose();
    _oldPasswordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Text(
                'Change password',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const ProfilePicture(),
              const UserName(),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        enableSuggestions: false,
                        autocorrect: false,
                        controller: _oldPasswordController,
                        decoration: const InputDecoration(
                          labelText: 'Actual Password',
                          filled: true,
                        ),
                        maxLines: 1,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              !RegExp(r"^[^\s]+$").hasMatch(value)) {
                            return 'Password is empty or contains whitecpaces';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        enableSuggestions: false,
                        autocorrect: false,
                        controller: _newPasswordController1,
                        decoration: const InputDecoration(
                          labelText: 'New Password',
                          filled: true,
                        ),
                        maxLines: 1,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              !RegExp(r"^[^\s]+$").hasMatch(value)) {
                            return 'Password is empty or contains whitecpaces';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        obscureText: true,
                        enableSuggestions: false,
                        autocorrect: false,
                        controller: _newPasswordController2,
                        decoration: const InputDecoration(
                          labelText: 'New Password Again',
                          filled: true,
                        ),
                        maxLines: 1,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              !RegExp(r"^[^\s]+$").hasMatch(value)) {
                            return 'Password is empty or contains whitecpaces';
                          }
                          return null;
                        },
                      ),
                    ),
                    MyCustomButton(
                      text: 'Save',
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          changePassword();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
