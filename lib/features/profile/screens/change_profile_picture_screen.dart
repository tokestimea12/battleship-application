import 'dart:io';
import 'dart:typed_data';

import 'package:battleship_application/providers/user_provider.dart';
import 'package:cropperx/cropperx.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

import '../../../common/my_custom_button.dart';
import '../services/profile_service.dart';
import '../widgets/profile_picture.dart';
import '../widgets/user_name.dart';

class ChangeProfilePictureScreen extends StatefulWidget {
  static const String routeName = '/change-profile-picture-screen';

  const ChangeProfilePictureScreen({Key? key}) : super(key: key);

  @override
  State<ChangeProfilePictureScreen> createState() =>
      _ChangeProfilePictureScreenState();
}

class _ChangeProfilePictureScreenState
    extends State<ChangeProfilePictureScreen> {
  File? _selectedImage;
  final ProfileService profileService = ProfileService();
  final GlobalKey _cropperKey = GlobalKey(debugLabel: 'cropperKey');

  void changeProfilePicture() async {
    Uint8List? croppedPicture = await Cropper.crop(cropperKey: _cropperKey);
    if (croppedPicture != null) {
      profileService.changeProfilePicture(
        context: context,
        userName:
            Provider.of<UserProvider>(context, listen: false).user.userName,
        newProfilePicture: croppedPicture,
      );

      setState(() {
        _selectedImage = null;
      });
    }
  }

  Future getImageGallery() async {
    XFile? image = await ImagePicker().pickImage(source: ImageSource.gallery);

    if (image != null) {
      setState(() {
        _selectedImage = File(image.path);
      });
    }
  }

  Future getImageCamera() async {
    XFile? image = await ImagePicker().pickImage(source: ImageSource.camera);

    if (image != null) {
      setState(() {
        _selectedImage = File(image.path);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Text(
                'Change profile picture',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              _selectedImage == null
                  ? Column(
                      children: const [
                        ProfilePicture(),
                        UserName(),
                      ],
                    )
                  : Container(),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  children: [
                    _selectedImage == null
                        ? Column(
                            children: [
                              MyCustomButton(
                                text: 'Upload from gallery',
                                onPressed: () => getImageGallery(),
                              ),
                              MyCustomButton(
                                text: 'Use camera',
                                onPressed: () => getImageCamera(),
                              ),
                            ],
                          )
                        : Column(
                            children: [
                              Cropper(
                                cropperKey: _cropperKey,
                                overlayType: OverlayType.circle,
                                image: Image.file(
                                  _selectedImage!,
                                ),
                              ),
                              MyCustomButton(
                                text: 'Save',
                                onPressed: () => changeProfilePicture(),
                              ),
                              MyCustomButton(
                                text: 'Choose another picture',
                                onPressed: () => setState(
                                  () {
                                    _selectedImage = null;
                                  },
                                ),
                              ),
                            ],
                          ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
