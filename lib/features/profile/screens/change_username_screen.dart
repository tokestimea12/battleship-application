import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/my_custom_button.dart';
import '../../../providers/user_provider.dart';
import '../services/profile_service.dart';
import '../widgets/profile_picture.dart';
import '../widgets/user_name.dart';

class ChangeUserNameScreen extends StatefulWidget {
  static const String routeName = '/change-username-screen';

  const ChangeUserNameScreen({Key? key}) : super(key: key);

  @override
  State<ChangeUserNameScreen> createState() => _ChangeUserNameScreenState();
}

class _ChangeUserNameScreenState extends State<ChangeUserNameScreen> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _newUserNameController = TextEditingController();

  final ProfileService profileService = ProfileService();

  void changeUserName() {
    profileService.changeUserName(
      context: context,
      oldUserName:
          Provider.of<UserProvider>(context, listen: false).user.userName,
      newUsername: _newUserNameController.text,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _newUserNameController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Text(
                'Change username',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              const ProfilePicture(),
              const UserName(),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: TextFormField(
                        controller: _newUserNameController,
                        decoration: const InputDecoration(
                          labelText: 'New Username',
                          filled: true,
                        ),
                        maxLines: 1,
                        validator: (value) {
                          if (value == null ||
                              value.isEmpty ||
                              !RegExp(r"^[a-zA-Z0-9]+$").hasMatch(value)) {
                            return 'Username should contain letters and numbers only';
                          }
                          return null;
                        },
                      ),
                    ),
                    MyCustomButton(
                      text: 'Save',
                      onPressed: () {
                        if (_formKey.currentState!.validate()) {
                          changeUserName();
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
