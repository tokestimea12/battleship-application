import 'package:battleship_application/common/my_custom_button.dart';
import 'package:battleship_application/features/profile/screens/change_password_screen.dart';
import 'package:battleship_application/features/profile/screens/change_username_screen.dart';
import 'package:battleship_application/features/profile/widgets/change_profile_picture.dart';
import 'package:battleship_application/features/profile/widgets/user_data_row.dart';
import 'package:battleship_application/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Column(
          children: [
            Text(
              'Profile',
              style: Theme.of(context).textTheme.titleLarge,
            ),
            const SizedBox(
              height: 15,
            ),
            const ChangeProfilePicture(),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: [
                  UserDataRow(
                      text: 'Username',
                      value: Provider.of<UserProvider>(context).user.userName),
                  UserDataRow(
                      text: 'E-mail',
                      value: Provider.of<UserProvider>(context).user.email),
                  MyCustomButton(
                    text: 'Change username',
                    onPressed: () => Navigator.of(context).pushNamed(
                      ChangeUserNameScreen.routeName,
                    ),
                  ),
                  MyCustomButton(
                    text: 'Change password',
                    onPressed: () => Navigator.of(context).pushNamed(
                      ChangePasswordScreen.routeName,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
