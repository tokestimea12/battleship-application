import 'dart:convert';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../constants/error_handling.dart';
import '../../../constants/global_variables.dart';
import '../../../constants/utils.dart';
import '../../../providers/user_provider.dart';
import '../../auth/screens/auth_screen.dart';

class ProfileService {
  void logOut(BuildContext context) async {
    try {
      SharedPreferences sharedPreferences =
          await SharedPreferences.getInstance();
      await sharedPreferences.setString('token', '');
      Navigator.of(context).pushNamedAndRemoveUntil(
        AuthScreen.routeName,
        (route) => false,
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  void changeUserName({
    required BuildContext context,
    required String oldUserName,
    required String newUsername,
  }) async {
    try {
      http.Response response = await http.patch(
        Uri.parse('$uri/api/users/$oldUserName/changeUserName'),
        body: json.encode({
          'newUserName': newUsername,
        }),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': Provider.of<UserProvider>(context, listen: false).user.token,
        },
      );

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () async {
          Provider.of<UserProvider>(context, listen: false)
              .setUserName(jsonDecode(response.body)['userName']);

          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('Success!'),
              content: const Text('Your username is now changed! :)'),
              actions: [
                TextButton(
                  child: const Text("OK"),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          );
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  void changePassword({
    required BuildContext context,
    required String userName,
    required String oldPassword,
    required String newPassword1,
    required String newPassword2,
  }) async {
    try {
      http.Response response = await http.patch(
        Uri.parse('$uri/api/users/$userName/changePassword'),
        body: json.encode({
          'oldPassword': oldPassword,
          'newPassword1': newPassword1,
          'newPassword2': newPassword2,
        }),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': Provider.of<UserProvider>(context, listen: false).user.token,
        },
      );

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () async {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('Success!'),
              content: const Text('Your password is now changed! :)'),
              actions: [
                TextButton(
                  child: const Text("OK"),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          );
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  void changeProfilePicture({
    required BuildContext context,
    required String userName,
    required Uint8List newProfilePicture,
  }) async {
    try {
      http.MultipartRequest request = http.MultipartRequest(
        'PATCH',
        Uri.parse('$uri/api/users/$userName/changeProfilePicture'),
      );

      Map<String, String> headers = {
        "Content-type": "multipart/form-data",
        'token': Provider.of<UserProvider>(context, listen: false).user.token,
      };

      request.files.add(
        http.MultipartFile.fromBytes(
          'newProfilePicture',
          newProfilePicture,
          filename: 'newProfilePicture',
          contentType: MediaType('image', 'png'),
        ),
      );
      request.headers.addAll(headers);

      var responseStream = await request.send();
      http.Response response = await http.Response.fromStream(responseStream);

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () async {
          Provider.of<UserProvider>(context, listen: false)
              .setProfilePicture(jsonDecode(response.body)['profilePicture']);

          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('Success!'),
              content: const Text('Your profile picture is now changed! :)'),
              actions: [
                TextButton(
                  child: const Text("OK"),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ],
            ),
          );
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }
}
