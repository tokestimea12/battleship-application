import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../constants/global_variables.dart';
import '../../../providers/user_provider.dart';
import '../screens/change_profile_picture_screen.dart';

class ChangeProfilePicture extends StatefulWidget {
  const ChangeProfilePicture({Key? key}) : super(key: key);

  @override
  State<ChangeProfilePicture> createState() => _ChangeProfilePictureState();
}

class _ChangeProfilePictureState extends State<ChangeProfilePicture>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 200));
    Timer(const Duration(milliseconds: 100), () {
      if (mounted) {
        _animationController.forward();
      }
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(1, 0),
        end: Offset.zero,
      ).animate(_animationController),
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: GlobalVariables.userCardBackgoundColor,
          border: Border.symmetric(
            horizontal: BorderSide(color: Theme.of(context).dividerColor),
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: InkWell(
            onTap: () => Navigator.of(context).pushNamed(
              ChangeProfilePictureScreen.routeName,
            ),
            child: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 10,
                        color: Theme.of(context).shadowColor,
                        offset: const Offset(0.5, 0.5),
                      ),
                    ],
                  ),
                  child: CircleAvatar(
                    backgroundImage: MemoryImage(
                      base64Decode(Provider.of<UserProvider>(context)
                          .user
                          .profilePicture),
                    ),
                    radius: 60,
                  ),
                ),
                const Positioned(
                  bottom: 0,
                  right: 100,
                  child: CircleAvatar(
                    child: Icon(Icons.edit_rounded),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
