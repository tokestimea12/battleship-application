import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../providers/user_provider.dart';

class ProfilePicture extends StatelessWidget {
  const ProfilePicture({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              boxShadow: [
                BoxShadow(
                  blurRadius: 10,
                  color: Theme.of(context).shadowColor,
                  offset: const Offset(0.5, 0.5),
                ),
              ],
            ),
            child: CircleAvatar(
              backgroundImage: MemoryImage(
                base64Decode(
                    Provider.of<UserProvider>(context).user.profilePicture),
              ),
              radius: 60,
            ),
          ),
        ],
      ),
    );
  }
}
