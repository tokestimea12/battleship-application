import 'package:flutter/material.dart';

class UserDataRow extends StatelessWidget {
  final String text;
  final String value;

  const UserDataRow({
    Key? key,
    required this.text,
    required this.value,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 8.0,
        right: 8.0,
        bottom: 16.0,
      ),
      child: InputDecorator(
        decoration: InputDecoration(
          labelText: text,
          labelStyle: Theme.of(context).textTheme.titleLarge,
          border: const OutlineInputBorder(),
        ),
        child: Text(
          value,
          style: Theme.of(context).textTheme.titleMedium,
        ),
      ),
    );
  }
}
