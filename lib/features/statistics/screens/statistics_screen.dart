import 'package:battleship_application/features/statistics/widgets/games_statistics_widget.dart';
import 'package:battleship_application/features/statistics/widgets/user_statistics_widget.dart';
import 'package:flutter/material.dart';

class StatisticsScreen extends StatelessWidget {
  const StatisticsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: [
          Text(
            'Statistics',
            style: Theme.of(context).textTheme.titleLarge,
          ),
          const SizedBox(
            height: 15,
          ),
          const UserStatistics(),
          const SizedBox(
            height: 15,
          ),
          const GamesStatistics(),
        ],
      ),
    );
  }
}
