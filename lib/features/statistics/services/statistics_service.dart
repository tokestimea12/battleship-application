import 'dart:convert';

import 'package:battleship_application/models/game_statistics.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import '../../../constants/error_handling.dart';
import '../../../constants/global_variables.dart';
import '../../../constants/utils.dart';
import '../../../providers/user_provider.dart';

class StatisticsService {
  Future<void> getGameStatistics({
    required BuildContext context,
    required String userName,
  }) async {
    try {
      http.Response response = await http.get(
        Uri.parse('$uri/api/users/$userName/statistics'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': Provider.of<UserProvider>(context, listen: false).user.token,
        },
      );

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () {
          Provider.of<UserProvider>(context, listen: false).user.points =
              jsonDecode(response.body)['points'];
          Provider.of<UserProvider>(context, listen: false).gamesLostForUser =
              jsonDecode(response.body)['lost'];
          Provider.of<UserProvider>(context, listen: false).gamesWonForUser =
              jsonDecode(response.body)['won'];
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }

  Future<void> getGamesForUser({
    required BuildContext context,
    required String userName,
  }) async {
    try {
      http.Response response = await http.get(
        Uri.parse('$uri/api/users/$userName/games'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'token': Provider.of<UserProvider>(context, listen: false).user.token,
        },
      );

      handleHttpError(
        response: response,
        context: context,
        onSuccess: () {
          Provider.of<UserProvider>(context, listen: false).gamesStatistics =
              [];
          for (var game in jsonDecode(response.body)['games']) {
            Provider.of<UserProvider>(context, listen: false)
                .gamesStatistics
                .add(GameStatistics.fromMap(game));
          }
        },
      );
    } catch (e) {
      showSnackbar(context, e.toString());
    }
  }
}
