import 'dart:convert';

import 'package:battleship_application/constants/global_variables.dart';
import 'package:battleship_application/features/statistics/services/statistics_service.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../models/game_statistics.dart';
import '../../../providers/user_provider.dart';

class GamesStatistics extends StatefulWidget {
  const GamesStatistics({Key? key}) : super(key: key);

  @override
  State<GamesStatistics> createState() => _GamesStatisticsState();
}

class _GamesStatisticsState extends State<GamesStatistics> {
  final StatisticsService _statisticsService = StatisticsService();
  late Future<void> _future;

  @override
  void initState() {
    super.initState();
    _future = _statisticsService.getGamesForUser(
        context: context,
        userName:
            Provider.of<UserProvider>(context, listen: false).user.userName);
  }

  Widget buildStatistics(int index) {
    UserProvider userProvider = Provider.of<UserProvider>(context);
    GameStatistics gameStatistics = userProvider.gamesStatistics[index];
    bool isWinner = gameStatistics.winnerUserName == userProvider.user.userName;

    int timeMinutes = (gameStatistics.gameTimeInSeconds / 60).floor();
    int timeSeconds = gameStatistics.gameTimeInSeconds % 60;

    return Card(
      color: isWinner
          ? GlobalVariables.wonGameColor
          : GlobalVariables.lostGameColor,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Column(
              children: [
                Text(
                    'Enemy: ${isWinner ? gameStatistics.loserUserName : gameStatistics.winnerUserName}'),
                timeMinutes != 0
                    ? Text('Game time: $timeMinutes min $timeSeconds s')
                    : Text('Game time: $timeSeconds s'),
                Text('Rounds: ${gameStatistics.numberOfRounds}'),
                Text(
                    'Points earned: ${isWinner ? gameStatistics.winnerPointsEarned : gameStatistics.loserPointsEarned}'),
                const SizedBox(height: 8),
                Text(gameStatistics.gameDate.toString().substring(0, 16)),
              ],
            ),
            const SizedBox(width: 8),
            Expanded(
              child: Column(
                children: [
                  Text(
                    'My fleet',
                    style: Theme.of(context).textTheme.labelMedium,
                  ),
                  Image.memory(
                    base64Decode(isWinner
                        ? gameStatistics.winnerFleetPicture
                        : gameStatistics.loserFleetPicture),
                  ),
                ],
              ),
            ),
            const SizedBox(width: 8),
            Expanded(
              child: Column(
                children: [
                  Text(
                    'Enemy fleet',
                    style: Theme.of(context).textTheme.labelMedium,
                    maxLines: 1,
                  ),
                  Image.memory(
                    base64Decode(isWinner
                        ? gameStatistics.loserFleetPicture
                        : gameStatistics.winnerFleetPicture),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InputDecorator(
          decoration: InputDecoration(
            labelText: 'GAMES',
            labelStyle: Theme.of(context).textTheme.headline5,
            border: const OutlineInputBorder(gapPadding: 4.0),
          ),
          child: FutureBuilder(
              future: _future,
              builder: ((context, snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasError) {
                    return const Text('We ran into an error');
                  }
                  return ListView.separated(
                      physics: const AlwaysScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, index) => buildStatistics(index),
                      separatorBuilder: ((context, index) => const SizedBox(
                            height: 10,
                          )),
                      itemCount: Provider.of<UserProvider>(context)
                          .gamesStatistics
                          .length);
                }
                return Container();
              })),
        ),
      ),
    );
  }
}
