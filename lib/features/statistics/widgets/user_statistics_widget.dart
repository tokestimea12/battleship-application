import 'dart:async';

import 'package:battleship_application/features/statistics/services/statistics_service.dart';
import 'package:battleship_application/providers/user_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/user_data.dart';
import '../../../constants/global_variables.dart';

class UserStatistics extends StatefulWidget {
  const UserStatistics({Key? key}) : super(key: key);

  @override
  State<UserStatistics> createState() => _UserStatisticsState();
}

class _UserStatisticsState extends State<UserStatistics>
    with TickerProviderStateMixin {
  StatisticsService statisticsServices = StatisticsService();
  late Future<void> future;
  late AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    future = statisticsServices.getGameStatistics(
        context: context,
        userName:
            Provider.of<UserProvider>(context, listen: false).user.userName);
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 300));
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _animationController.reset();
    Timer(const Duration(milliseconds: 100), () {
      if (mounted) {
        _animationController.forward();
      }
    });
    UserProvider userProvider = Provider.of<UserProvider>(context);
    return SlideTransition(
      position: Tween<Offset>(
        begin: const Offset(1, 0),
        end: Offset.zero,
      ).animate(_animationController),
      child: Container(
        decoration: BoxDecoration(
          color: GlobalVariables.userCardBackgoundColor,
          border: Border.symmetric(
            horizontal: BorderSide(color: Theme.of(context).dividerColor),
          ),
        ),
        child: FutureBuilder(
            future: future,
            builder: ((context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return const Text('We ran into an error');
                }
                return Container(
                  decoration: BoxDecoration(
                    border: Border(
                      right: BorderSide(
                        width: 2.0,
                        color: Theme.of(context).dividerColor,
                      ),
                    ),
                  ),
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border(
                              right: BorderSide(
                                width: 2.0,
                                color: Theme.of(context).dividerColor,
                              ),
                            ),
                          ),
                          child: UserData(
                            profilePicture: userProvider.user.profilePicture,
                            userName: userProvider.user.userName,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const SizedBox(
                              height: 25,
                            ),
                            Text(
                              'POINTS: ${userProvider.user.points}',
                              style: Theme.of(context).textTheme.headline6,
                            ),
                            const SizedBox(
                              height: 25,
                            ),
                            Table(
                              border: TableBorder.symmetric(
                                inside: BorderSide(
                                    color: Theme.of(context).dividerColor),
                              ),
                              defaultVerticalAlignment:
                                  TableCellVerticalAlignment.middle,
                              children: [
                                TableRow(
                                  decoration: BoxDecoration(
                                      color: Theme.of(context)
                                          .secondaryHeaderColor),
                                  children: [
                                    TableCell(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Center(
                                          child: Text(
                                            'WON',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline6,
                                          ),
                                        ),
                                      ),
                                    ),
                                    TableCell(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Center(
                                          child: Text(
                                            'LOST',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline6,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                TableRow(
                                  decoration: BoxDecoration(
                                      color: Theme.of(context).primaryColor),
                                  children: [
                                    TableCell(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Center(
                                          child: Text(
                                            '${userProvider.gamesWonForUser}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline6,
                                          ),
                                        ),
                                      ),
                                    ),
                                    TableCell(
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Center(
                                          child: Text(
                                            '${userProvider.gamesLostForUser}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline6,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }
              return Container();
            })),
      ),
    );
  }
}
