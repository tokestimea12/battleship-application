import 'package:battleship_application/providers/game_statistics_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

import 'common/navigation_bar.dart';
import 'constants/global_variables.dart';
import 'features/auth/screens/auth_screen.dart';
import 'features/auth/services/auth_service.dart';
import 'providers/room_provider.dart';
import 'providers/user_provider.dart';
import 'router.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(
      create: (context) => UserProvider(),
    ),
    ChangeNotifierProvider(
      create: (context) => RoomProvider(),
    ),
    ChangeNotifierProvider(
      create: (context) => GameStatisticsProvider(),
    ),
  ], child: const MyApp()));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final AuthService authService = AuthService();

  @override
  void initState() {
    super.initState();
    authService.getUserData(context);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Battleship',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: GlobalVariables.seedColor,
        ),
        scaffoldBackgroundColor: Theme.of(context).backgroundColor,
        fontFamily: 'Orbitron',
      ),
      onGenerateRoute: (settings) => generateRoute(settings),
      home: Provider.of<UserProvider>(context).user.token.isNotEmpty
          ? const AppNavigationBar()
          : const AuthScreen(),
    );
  }
}
