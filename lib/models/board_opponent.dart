import 'package:battleship_application/features/game/logic/ship.dart';
import 'package:battleship_application/features/game/logic/tile.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';

import '../features/game/logic/ship_shot_component.dart';

class BoardOpponent {
  int? boardSize;
  Set<TileOpponent> tiles = {};
  List<ShipOpponent> ships = [];
  Set<ShipShotComponent> goodShotSprites = {};

  int get numberOfTiles => boardSize! * boardSize!;

  set setBoardSize(int size) {
    boardSize = size;
  }

  void addBoard(double tileSize) {
    tiles = {};

    for (int i = 0; i < numberOfTiles; i++) {
      Rect rect = Rect.fromLTWH(
        (i % boardSize!) * tileSize,
        (i / boardSize!).floor() * tileSize,
        tileSize,
        tileSize,
      );

      TileOpponent tile = TileOpponent(
        id: i,
        tileSize: tileSize,
        position: Vector2(rect.left, rect.top),
      );

      ShipShotComponent shipShotComponent = ShipShotComponent(
          tileOpponent: tile,
          tileSize: tileSize,
          position: Vector2(rect.left, rect.top));
      tiles.add(tile);
      goodShotSprites.add(shipShotComponent);
    }
  }

  void addShips() {
    ships = [];

    if (boardSize == 10) {
      _addShips10();
    } else if (boardSize == 8) {
      _addShips8();
    } else {
      _addShips12();
    }
  }

  void _addShips10() {
    ShipOpponent ship2 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 84),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship2);

    ShipOpponent ship31 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 72),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship31);

    ShipOpponent ship32 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 73),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship32);

    ShipOpponent ship4 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 61),
      imageName: "ship4.png",
      length: 4,
    );

    ships.add(ship4);

    ShipOpponent ship5 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 50),
      imageName: "ship5.png",
      length: 5,
    );

    ships.add(ship5);
  }

  void _addShips8() {
    ShipOpponent ship21 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 52),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship21);

    ShipOpponent ship22 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 53),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship22);

    ShipOpponent ship31 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 43),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship31);

    ShipOpponent ship32 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 42),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship32);

    ShipOpponent ship33 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 41),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship33);

    ShipOpponent ship4 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 32),
      imageName: "ship4.png",
      length: 4,
    );

    ships.add(ship4);
  }

  void _addShips12() {
    ShipOpponent ship21 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 125),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship21);

    ShipOpponent ship22 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 126),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship22);

    ShipOpponent ship31 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 111),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship31);

    ShipOpponent ship32 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 112),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship32);

    ShipOpponent ship41 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 97),
      imageName: "ship4.png",
      length: 4,
    );

    ships.add(ship41);

    ShipOpponent ship42 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 98),
      imageName: "ship4.png",
      length: 4,
    );

    ships.add(ship42);

    ShipOpponent ship5 = ShipOpponent(
      startTile: tiles.singleWhere((tile) => tile.id == 84),
      imageName: "ship5.png",
      length: 5,
    );

    ships.add(ship5);
  }

  void setShipAsDestroyed(
    int length,
    double positionX,
    double positionY,
    double angle,
  ) {
    for (ShipOpponent ship in ships) {
      if (!ship.destroyed && ship.length == length) {
        ship.setShipData(positionX, positionY, angle);
        break;
      }
    }
  }
}
