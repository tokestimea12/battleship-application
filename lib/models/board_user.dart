import 'package:battleship_application/features/game/logic/ship_shot_component.dart';
import 'package:battleship_application/features/game/logic/ship_user.dart';
import 'package:battleship_application/features/game/logic/tile_user.dart';
import 'package:flame/components.dart';
import 'package:flame/game.dart';
import 'package:flutter/material.dart';

class BoardUser {
  int? boardSize;
  Set<TileUser> tiles = {};
  List<ShipUser> ships = [];
  Set<ShipShotComponent> goodShotSprites = {};

  int get numberOfTiles => boardSize! * boardSize!;

  set setBoardSize(int size) {
    boardSize = size;
  }

  void addBoard(double tileSize) {
    tiles = {};

    for (int i = 0; i < numberOfTiles; i++) {
      Rect rect = Rect.fromLTWH(
        (i % boardSize!) * tileSize,
        (i / boardSize!).floor() * tileSize,
        tileSize,
        tileSize,
      );

      TileUser tile = TileUser(
        id: i,
        tileSize: tileSize,
        position: Vector2(rect.left, rect.top),
      );

      ShipShotComponent shipShotComponent = ShipShotComponent(
          tileUser: tile,
          tileSize: tileSize,
          position: Vector2(rect.left, rect.top));
      tiles.add(tile);
      goodShotSprites.add(shipShotComponent);
    }
  }

  void addShips() {
    ships = [];
    if (boardSize == 10) {
      _addShips10();
    } else if (boardSize == 8) {
      _addShips8();
    } else {
      _addShips12();
    }
  }

  void _addShips10() {
    ShipUser ship2 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 84),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship2);

    ShipUser ship31 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 72),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship31);

    ShipUser ship32 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 73),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship32);

    ShipUser ship4 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 61),
      imageName: "ship4.png",
      length: 4,
    );

    ships.add(ship4);

    ShipUser ship5 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 50),
      imageName: "ship5.png",
      length: 5,
    );

    ships.add(ship5);
  }

  void _addShips8() {
    ShipUser ship21 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 52),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship21);

    ShipUser ship22 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 53),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship22);

    ShipUser ship31 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 43),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship31);

    ShipUser ship32 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 42),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship32);

    ShipUser ship33 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 41),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship33);

    ShipUser ship4 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 32),
      imageName: "ship4.png",
      length: 4,
    );

    ships.add(ship4);
  }

  void _addShips12() {
    ShipUser ship21 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 125),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship21);

    ShipUser ship22 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 126),
      imageName: "ship.png",
      length: 2,
    );

    ships.add(ship22);

    ShipUser ship31 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 111),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship31);

    ShipUser ship32 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 112),
      imageName: "ship3.png",
      length: 3,
    );

    ships.add(ship32);

    ShipUser ship41 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 97),
      imageName: "ship4.png",
      length: 4,
    );

    ships.add(ship41);

    ShipUser ship42 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 98),
      imageName: "ship4.png",
      length: 4,
    );

    ships.add(ship42);

    ShipUser ship5 = ShipUser(
      startTile: tiles.singleWhere((tile) => tile.id == 84),
      imageName: "ship5.png",
      length: 5,
    );

    ships.add(ship5);
  }
}
