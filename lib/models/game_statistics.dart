import 'dart:convert';

class GameStatistics {
  String winnerUserName;
  String loserUserName;
  int gameTimeInSeconds;
  int numberOfRounds;
  String winnerFleetPicture;
  String loserFleetPicture;
  int winnerPointsEarned;
  int loserPointsEarned;
  DateTime gameDate;

  GameStatistics({
    required this.winnerUserName,
    required this.loserUserName,
    required this.gameTimeInSeconds,
    required this.numberOfRounds,
    required this.winnerFleetPicture,
    required this.loserFleetPicture,
    required this.winnerPointsEarned,
    required this.loserPointsEarned,
    required this.gameDate,
  });

  Map<String, dynamic> toMap() {
    return {
      'winnerUserName': winnerUserName,
      'loserUserName': loserUserName,
      'gameTimeInSeconds': gameTimeInSeconds,
      'numberOfRounds': numberOfRounds,
      'winnerFleetPicture': winnerFleetPicture,
      'loserFleetPicture': loserFleetPicture,
      'winnerPointsEarned': winnerPointsEarned,
      'loserPointsEarned': loserPointsEarned,
      'gameDate': gameDate.toIso8601String(),
    };
  }

  factory GameStatistics.fromMap(Map<String, dynamic> map) {
    return GameStatistics(
      winnerUserName: map['winnerUserName'] ?? '',
      loserUserName: map['loserUserName'] ?? '',
      gameTimeInSeconds: map['gameTimeInSeconds']?.toInt() ?? 0,
      numberOfRounds: map['numberOfRounds']?.toInt() ?? 0,
      winnerFleetPicture: map['winnerFleetPicture'] ?? '',
      loserFleetPicture: map['loserFleetPicture'] ?? '',
      winnerPointsEarned: map['winnerPointsEarned']?.toInt() ?? 0,
      loserPointsEarned: map['loserPointsEarned']?.toInt() ?? 0,
      gameDate: DateTime.tryParse(map['gameDate']) ?? DateTime.now(),
    );
  }

  String toJson() => json.encode(toMap());

  factory GameStatistics.fromJson(String source) =>
      GameStatistics.fromMap(json.decode(source));
}
