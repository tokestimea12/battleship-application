import 'dart:typed_data';

import 'package:battleship_application/models/ship.dart';

import '../constants/utils.dart';

class Room {
  String roomId;
  String player1;
  String player2;
  String player1Picture;
  String player2Picture;
  int player1Points;
  int player2Points;
  int boardSize;
  Turn turn;
  bool gameStarted;
  int round;
  List<Ship> player1Ships;
  List<Ship> player2Ships;
  int gameTime;
  Uint8List? finalFleetPlayer1;
  Uint8List? finalFleetPlayer2;

  Room({
    required this.roomId,
    required this.player1,
    required this.player2,
    required this.player1Picture,
    required this.player2Picture,
    required this.player1Points,
    required this.player2Points,
    required this.boardSize,
    required this.turn,
    required this.gameStarted,
    required this.round,
    required this.player1Ships,
    required this.player2Ships,
    required this.gameTime,
  });
}
