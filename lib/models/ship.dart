class Ship {
  int length;
  bool isDestroyed;

  Ship({
    required this.length,
    required this.isDestroyed,
  });
}
