import 'dart:convert';

class User {
  final int userId;
  String userName;
  final String email;
  final String pswd1;
  final String pswd2;
  final String token;
  String profilePicture;
  int points;

  User({
    required this.userId,
    required this.userName,
    required this.email,
    required this.pswd1,
    required this.pswd2,
    required this.token,
    required this.profilePicture,
    required this.points,
  });

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'userName': userName,
      'email': email,
      'pswd1': pswd1,
      'pswd2': pswd2,
      'token': token,
      'profilePicture': profilePicture,
      'points': points,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      userId: map['userId']?.toInt() ?? 0,
      userName: map['userName'] ?? '',
      email: map['email'] ?? '',
      pswd1: map['pswd1'] ?? '',
      pswd2: map['pswd2'] ?? '',
      token: map['token'] ?? '',
      profilePicture: map['profilePicture'] ?? '',
      points: map['points']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));
}
