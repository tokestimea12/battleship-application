import 'package:flutter/material.dart';

import '../constants/utils.dart';
import '../features/game/logic/tile.dart';
import '../features/game/logic/tile_user.dart';
import '../models/board_opponent.dart';
import '../models/board_user.dart';

class GameModel with ChangeNotifier {
  int boardSize;
  bool _started = false;
  bool _over = false;
  BoardUser boardUser = BoardUser();
  BoardOpponent boardOpponent = BoardOpponent();
  Turn _turn = Turn.player1;

  GameModel({required this.boardSize});

  set gameOver(bool over) {
    _over = over;
    notifyListeners();
  }

  bool get gameOver => _over;

  set gameStarted(bool started) {
    _started = started;
    notifyListeners();
  }

  bool get gameStarted => _started;

  void changeTurn() {
    if (_turn == Turn.player1) {
      _turn = Turn.player2;
    } else {
      _turn = Turn.player1;
    }
    notifyListeners();
  }

  Turn get turn => _turn;

  void initGame(double tileSize) {
    _started = false;
    _over = false;
    _turn = Turn.player1;
    boardUser.boardSize = boardSize;
    boardUser.addBoard(tileSize);
    boardUser.addShips();

    boardOpponent.boardSize = boardSize;
    boardOpponent.addBoard(tileSize);
    boardOpponent.addShips();
  }

  bool changeTappedTileForUser(int tileId) {
    for (TileUser tile in boardUser.tiles) {
      if (tile.id == tileId) {
        tile.tapped = true;
        notifyListeners();
        if (tile.ship == null) {
          return false;
        }
        tile.ship!.isShipDestroyed();
        return true;
      }
    }
    return false;
  }

  void setShipIsHereForOpponentTile(int tileId) {
    for (TileOpponent tile in boardOpponent.tiles) {
      if (tile.id == tileId) {
        tile.isShipHere = true;
        notifyListeners();
        break;
      }
    }
  }
}
