import 'package:battleship_application/models/game_statistics.dart';
import 'package:flutter/material.dart';

class GameStatisticsProvider extends ChangeNotifier {
  GameStatistics _gameStatistics = GameStatistics(
    winnerUserName: '',
    loserUserName: '',
    gameTimeInSeconds: 0,
    numberOfRounds: 0,
    winnerFleetPicture: '',
    loserFleetPicture: '',
    winnerPointsEarned: 0,
    loserPointsEarned: 0,
    gameDate: DateTime.now(),
  );

  int _gameId = 0;

  int get gameId => _gameId;

  set gameId(int id) {
    _gameId = id;
    notifyListeners();
  }

  GameStatistics get gameStatistics => _gameStatistics;

  void setGameStatistics(String recentGameStatistics) {
    _gameStatistics = GameStatistics.fromJson(recentGameStatistics);

    notifyListeners();
  }

  void setWinnerFleetPicture(String winnerFleetPicture) {
    _gameStatistics.winnerFleetPicture = winnerFleetPicture;

    notifyListeners();
  }

  void setLoserFleetPicture(String loserFleetPicture) {
    _gameStatistics.loserFleetPicture = loserFleetPicture;

    notifyListeners();
  }
}
