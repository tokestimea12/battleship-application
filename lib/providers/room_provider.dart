import 'dart:typed_data';

import 'package:battleship_application/constants/utils.dart';
import 'package:battleship_application/models/room.dart';
import 'package:flutter/cupertino.dart';

import '../models/ship.dart';

class RoomProvider extends ChangeNotifier {
  final Room _room = Room(
    roomId: '',
    player1: '',
    player2: '',
    player1Picture: '',
    player2Picture: '',
    boardSize: 0,
    turn: Turn.player1,
    gameStarted: false,
    round: 1,
    player1Points: 0,
    player2Points: 0,
    player1Ships: [],
    player2Ships: [],
    gameTime: 0,
  );

  bool _turnChanged = false;

  bool get turnChanged => _turnChanged;

  Room get room => _room;

  set finalFleetPlayer1(Uint8List uint8list) {
    _room.finalFleetPlayer1 = uint8list;
    notifyListeners();
  }

  void setRoom(
    String roomId,
    String player1,
    String player2,
    int boardSize,
    int player1Points,
    int player2Points,
  ) {
    _room.roomId = roomId;
    _room.player1 = player1;
    _room.player2 = player2;
    _room.boardSize = boardSize;
    _room.player1Points = player1Points;
    _room.player2Points = player2Points;
    _room.gameTime = 0;
    _room.gameStarted = false;
    _room.turn = Turn.player1;

    _room.player1Ships = [];
    _room.player2Ships = [];

    if (boardSize == 8) {
      _room.player1Ships.add(Ship(
        length: 4,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));

      _room.player2Ships.add(Ship(
        length: 4,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));
    } else if (boardSize == 10) {
      _room.player1Ships.add(Ship(
        length: 5,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 4,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));

      _room.player2Ships.add(Ship(
        length: 5,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 4,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));
    } else {
      _room.player1Ships.add(Ship(
        length: 5,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 4,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 4,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));
      _room.player1Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));

      _room.player2Ships.add(Ship(
        length: 5,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 4,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 4,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 3,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));
      _room.player2Ships.add(Ship(
        length: 2,
        isDestroyed: false,
      ));
    }

    notifyListeners();
  }

  void setProfilePicture1(String picture) {
    _room.player1Picture = picture;

    notifyListeners();
  }

  void setProfilePicture2(String picture) {
    _room.player2Picture = picture;

    notifyListeners();
  }

  void inversePlayers() {
    String player2 = _room.player1;
    String player2Picture = _room.player1Picture;
    _room.player1 = _room.player2;
    _room.player1Picture = _room.player2Picture;
    _room.player2 = player2;
    _room.player2Picture = player2Picture;

    notifyListeners();
  }

  set gameStarted(bool started) {
    _room.gameStarted = started;
    notifyListeners();
  }

  bool get gameStarted => _room.gameStarted;

  void changeTurn() {
    if (_room.turn == Turn.player1) {
      _room.turn = Turn.player2;
    } else {
      _room.turn = Turn.player1;
    }
    _turnChanged = true;
    notifyListeners();
  }

  void setTurnChangedToFalse() {
    if (_turnChanged == true) {
      _turnChanged = false;
      notifyListeners();
    }
  }

  void incRound() {
    _room.round++;

    notifyListeners();
  }

  void setUserShipAsDestroyed(int length) {
    for (Ship ship in _room.player1Ships) {
      if (!ship.isDestroyed && ship.length == length) {
        ship.isDestroyed = true;
        notifyListeners();
        break;
      }
    }
  }

  void setOpponentShipAsDestroyed(int length) {
    for (Ship ship in _room.player2Ships) {
      if (!ship.isDestroyed && ship.length == length) {
        ship.isDestroyed = true;
        notifyListeners();
        break;
      }
    }
  }

  void incGameTime() {
    _room.gameTime++;
  }
}
