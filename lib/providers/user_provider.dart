import 'package:battleship_application/models/game_statistics.dart';
import 'package:battleship_application/models/user.dart';
import 'package:flutter/material.dart';

class UserProvider extends ChangeNotifier {
  User _user = User(
    userId: 0,
    userName: '',
    email: '',
    pswd1: '',
    pswd2: '',
    token: '',
    profilePicture: '',
    points: 0,
  );

  int _gamesWonForUser = 0;
  int _gamesLostForUser = 0;

  List<GameStatistics> gamesStatistics = [];

  List<User> leaderboard = [];

  User get user => _user;

  void setUser(String user) {
    _user = User.fromJson(user);

    notifyListeners();
  }

  void setUserName(String userName) {
    _user.userName = userName;

    notifyListeners();
  }

  void setProfilePicture(String profilePicture) {
    _user.profilePicture = profilePicture;

    notifyListeners();
  }

  void incPoints(int points) {
    _user.points += points;

    notifyListeners();
  }

  int get gamesWonForUser => _gamesWonForUser;
  int get gamesLostForUser => _gamesLostForUser;

  set gamesWonForUser(int nr) {
    _gamesWonForUser = nr;

    notifyListeners();
  }

  set gamesLostForUser(int nr) {
    _gamesLostForUser = nr;

    notifyListeners();
  }
}
