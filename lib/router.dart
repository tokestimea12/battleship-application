import 'package:battleship_application/features/game/screens/enter_room_screen.dart';
import 'package:flutter/material.dart';

import 'common/navigation_bar.dart';
import 'features/auth/screens/auth_screen.dart';
import 'features/auth/screens/login_screen.dart';
import 'features/auth/screens/signup_screen.dart';
import 'features/game/screens/game_chooser_screen.dart';
import 'features/game/screens/game_over_screen.dart';
import 'features/game/screens/game_screen.dart';
import 'features/profile/screens/change_password_screen.dart';
import 'features/profile/screens/change_profile_picture_screen.dart';
import 'features/profile/screens/change_username_screen.dart';

Route<dynamic> generateRoute(RouteSettings routeSettings) {
  switch (routeSettings.name) {
    case AuthScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const AuthScreen(),
      );
    case SignUpScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const SignUpScreen(),
      );
    case LogInScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const LogInScreen(),
      );
    case AppNavigationBar.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const AppNavigationBar(),
      );
    case ChangeUserNameScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const ChangeUserNameScreen(),
      );
    case ChangePasswordScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const ChangePasswordScreen(),
      );
    case ChangeProfilePictureScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const ChangeProfilePictureScreen(),
      );
    case GameChooserScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const GameChooserScreen(),
      );
    case EnterRoomScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const EnterRoomScreen(),
      );
    case GameScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const GameScreen(),
      );
    case GameOverScreen.routeName:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const GameOverScreen(),
      );
    default:
      return MaterialPageRoute(
        settings: routeSettings,
        builder: (_) => const Scaffold(
          body: Center(
            child: Text('Wrong page!'),
          ),
        ),
      );
  }
}
