import mysql from "mysql2";
import util from "util";

//connect to database
const connectionPool = mysql.createPool({
  host: "localhost",
  port: 3306,
  user: "ttim1972",
  password: "ttim1972",
  database: "battleship",
  connectionLimit: 4,
});

const dbQuery = util.promisify(connectionPool.query).bind(connectionPool);

export const insertUser = (user) => {
  const queryStr = "INSERT INTO Users VALUES (default, ?, ?, ?, ?, ?);";
  return dbQuery(queryStr, [
    user.email,
    user.userName,
    user.userPswd,
    0,
    "Images\\blank_profile_picture.png",
  ]);
};

export const selectUserName = async (userId) => {
  const queryStr = "SELECT userName FROM Users WHERE userId=?";
  const res = await dbQuery(queryStr, [userId]);
  return res[0].userName;
};

export const selectUserIdByName = async (userName) => {
  const queryStr = "SELECT userId FROM Users WHERE userName=?";
  const res = await dbQuery(queryStr, [userName]);
  if (res.length > 0) {
    return res[0].userId;
  }
  return -1;
};

export const selectUserIdByEmail = async (email) => {
  const queryStr = "SELECT userId FROM Users WHERE email=?";
  const res = await dbQuery(queryStr, [email]);
  if (res.length > 0) {
    return res[0].userId;
  }
  return -1;
};

export const selectUser = async (userId) => {
  const queryStr = "SELECT * FROM Users WHERE userId=?";
  const res = await dbQuery(queryStr, [userId]);
  return res[0];
};

export const updateUserName = async (oldUserName, newUserName) => {
  const queryStr = "UPDATE Users SET userName=? WHERE userName=?";
  const res = await dbQuery(queryStr, [newUserName, oldUserName]);
  return res;
};

export const selectPasswordByUser = async (userId) => {
  const queryStr = "SELECT userPswd FROM Users WHERE userId=?";
  const res = await dbQuery(queryStr, [userId]);
  if (res.length > 0) {
    return res[0].userPswd;
  }
  return -1;
};

export const updatePasswordForUser = async (userId, newPassword) => {
  const queryStr = "UPDATE Users SET userPswd=? WHERE userId=?";
  const res = await dbQuery(queryStr, [newPassword, userId]);
  return res;
};

export const updateProfilePictureForUser = async (
  userId,
  newProfilePicture
) => {
  const queryStr = "UPDATE Users SET profilePicture=? WHERE userId=?";
  const res = await dbQuery(queryStr, [newProfilePicture, userId]);
  return res;
};

export const updatePointsForUser = async (userId, points) => {
  const queryStr = "UPDATE Users SET points=points+? WHERE userId=?";
  const res = await dbQuery(queryStr, [points, userId]);
  return res;
};

export const selectProfilePictureByUser = async (userId) => {
  const queryStr = "SELECT profilePicture FROM Users WHERE userId=?";
  const res = await dbQuery(queryStr, [userId]);
  if (res.length > 0) {
    return res[0].profilePicture;
  }
  return -1;
};

export const selectPointsByUser = async (userName) => {
  const queryStr = "SELECT points FROM Users WHERE userName=?";
  const res = await dbQuery(queryStr, [userName]);
  if (res.length > 0) {
    return res[0].points;
  }
  return -1;
};

export const insertGame = (game) => {
  const queryStr =
    "INSERT INTO Games VALUES (default, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
  return dbQuery(queryStr, [
    game.winnerUserId,
    game.loserUserId,
    game.gameTimeInSeconds,
    game.numberOfRounds,
    "",
    "",
    game.winnerPointsEarned,
    game.loserPointsEarned,
    game.gameDate,
  ]);
};

export const uploadWinnerFleetPicture = async (gameId, winnerFleetPicture) => {
  const queryStr = "UPDATE Games SET winnerFleetPicture=? WHERE gameId=?";
  const res = await dbQuery(queryStr, [winnerFleetPicture, gameId]);
  return res;
};

export const uploadLoserFleetPicture = async (gameId, loserFleetPicture) => {
  const queryStr = "UPDATE Games SET loserFleetPicture=? WHERE gameId=?";
  const res = await dbQuery(queryStr, [loserFleetPicture, gameId]);
  return res;
};

export const selectWinnerNameByGameId = async (gameId) => {
  const queryStr =
    "SELECT userName FROM Users JOIN Games ON Users.userId = Games.winnerUserId WHERE Games.gameId=?";
  const res = await dbQuery(queryStr, [gameId]);
  if (res.length > 0) {
    return res[0].userName;
  }
  return -1;
};

export const selectWinnerFleetPictureByGameId = async (gameId) => {
  const queryStr = "SELECT winnerFleetPicture FROM Games WHERE gameId=?";
  const res = await dbQuery(queryStr, [gameId]);
  if (res.length > 0) {
    return res[0].winnerFleetPicture;
  }
  return -1;
};

export const selectLoserFleetPictureByGameId = async (gameId) => {
  const queryStr = "SELECT loserFleetPicture FROM Games WHERE gameId=?";
  const res = await dbQuery(queryStr, [gameId]);
  if (res.length > 0) {
    return res[0].loserFleetPicture;
  }
  return -1;
};

export const selectGame = async (gameId) => {
  const queryStr = "SELECT * FROM Games WHERE gameId=?";
  const res = await dbQuery(queryStr, [gameId]);
  return res[0];
};

export const selectWonGamesForUser = async (userId) => {
  const queryStr = "SELECT COUNT(*) as nr FROM Games WHERE winnerUserId=?";
  const res = await dbQuery(queryStr, [userId]);
  return res[0].nr;
};

export const selectLostGamesForUser = async (userId) => {
  const queryStr = "SELECT COUNT(*) as nr FROM Games WHERE loserUserId=?";
  const res = await dbQuery(queryStr, [userId]);
  return res[0].nr;
};

export const selectAllGamesForUser = async (userId) => {
  const queryStr =
    "SELECT * FROM Games WHERE loserUserId=? OR winnerUserId=? ORDER BY gameDate DESC";
  const res = await dbQuery(queryStr, [userId, userId]);
  return res;
};

export const selectLeaderboard = async () => {
  const queryStr = "SELECT * FROM Users ORDER BY points DESC LIMIT 20";
  const res = await dbQuery(queryStr);
  return res;
};
