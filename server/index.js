import express from "express";
import morgan from "morgan";
import http from "http";
import { Server } from "socket.io";
import authRoutes from "./routes/auth.js";
import apiUserRoutes from "./routes/users.js";
import apiGameRoutes from "./routes/games.js";
import { authForSocket } from "./middleware/auth.js";
import Room from "./model/room.js";
import { selectPointsByUser } from "./db/connection.js";
import { generateGameId } from "./utils/gameIdGenerator.js";

const app = express();

const port = process.env.PORT || 8000;

app.use(morgan("tiny"));
app.use(express.static("static"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/", authRoutes);
app.use("/api", apiUserRoutes);
app.use("/api", apiGameRoutes);

const server = http.createServer(app); 
global.io = new Server(server);

let rooms = [];

io.use(authForSocket).on("connection", (socket) => {
  socket.on("connectToGame", async ({ userName, boardSize }) => {
    let i = 0;
    while (i < rooms.length) {
      if (
        rooms[i].player2 == undefined &&
        rooms[i].boardSize == boardSize &&
        rooms[i].player1 !== userName
      ) {
        rooms[i].player2 = userName;
        socket.join(rooms[i].roomId);
        try {
          const player1Points = await selectPointsByUser(rooms[i].player1);
          const player2Points = await selectPointsByUser(rooms[i].player2);
          io.to(rooms[i].roomId).emit("opponentJoined", {
            ...rooms[i],
            player1Points: player1Points,
            player2Points: player2Points,
          });
        } catch (error) {
          io.to(rooms[i].roomId).emit("error", "Internal server error");
        }
        break;
      }
      i++;
    }

    if (i === rooms.length) {
      while (true) {
        const gameId = generateGameId();
        if (!rooms.some((room) => room.roomId === gameId)) {
          rooms.push(new Room(gameId, userName, boardSize));
          socket.join(gameId);
          break;
        }
      }
    }
  });

  socket.on("createRoom", async ({ userName, boardSize }) => {
    while (true) {
      const gameId = generateGameId();
      if (!rooms.some((room) => room.roomId === gameId)) {
        rooms.push(new Room(gameId, userName, boardSize));
        socket.join(gameId);

        io.to(gameId).emit("roomCreated", gameId);
        break;
      }
    }
  });

  socket.on("roomEntered", async ({ userName, roomId }) => {
    let i = 0;
    while (i < rooms.length) {
      if (rooms[i].roomId == roomId) {
        if (rooms[i].player2 == undefined) {
          rooms[i].player2 = userName;
          socket.join(roomId);
          try {
            const player1Points = await selectPointsByUser(rooms[i].player1);
            const player2Points = await selectPointsByUser(rooms[i].player2);
            io.to(rooms[i].roomId).emit("opponentJoined", {
              ...rooms[i],
              player1Points: player1Points,
              player2Points: player2Points,
            });
          } catch (error) {
            io.to(rooms[i].roomId).emit("error", "Internal server error");
            break;
          }
          break;
        } else {
          socket.emit("error", "Room is unavailable! Try again later.");
          break;
        }
      }
      i++;
    }
    if (i == rooms.length) {
      socket.emit("error", "Room is unavailable! Try again later.");
    }
  });

  socket.on("userDoneWaiting", async ({ userName }) => {
    let i = 0;
    let roomId;
    while (i < rooms.length) {
      if (rooms[i].player1 == userName) {
        roomId = rooms[i].roomId;
        rooms.splice(i, 1);
        break;
      }
      i++;
    }
    const clients = io.sockets.adapter.rooms.get(roomId);

    for (const clientId of clients) {
      const clientSocket = io.sockets.sockets.get(clientId);
      clientSocket.leave(roomId);
    }
  });

  socket.on("gameCanStart", async ({ userName, roomId }) => {
    let i = 0;
    while (i < rooms.length) {
      if (rooms[i].roomId == roomId) {
        rooms[i].gameCanStart++;
        if (rooms[i].gameCanStart === 1) {
          io.to(roomId).emit("waitingForOpponentToStart", userName);
        } else if (rooms[i].gameCanStart === 2) {
          io.to(roomId).emit("startingGame", userName);
        }
        break;
      }
      i++;
    }
  });

  socket.on("changeTurn", async ({ roomId }) => {
    io.to(roomId).emit("changeHasToTurn");
  });

  socket.on("attacked", async ({ roomId, tileId }) => {
    io.to(roomId).emit("attackReceived", tileId);
  });

  socket.on("goodShot", async ({ roomId, tileId }) => {
    io.to(roomId).emit("goodShotReceived", tileId);
  });

  socket.on(
    "shipDestroyed",
    async ({ roomId, length, positionX, positionY, angle }) => {
      io.to(roomId).emit("shipDestroyedReceived", {
        length,
        positionX,
        positionY,
        angle,
      });
    }
  );

  socket.on("gameOver", async ({ roomId, gameId, cause }) => {
    io.to(roomId).emit("gameOver", { gameId, cause });
  });

  socket.on("fleetPictureUploaded", async ({ roomId }) => {
    let i = 0;
    while (i < rooms.length) {
      if (rooms[i].roomId == roomId) {
        rooms[i].nrOfUploadedPictures++;
        if (rooms[i].nrOfUploadedPictures == 2) {
          io.to(roomId).emit("bothPicturesUploaded");

          const clients = io.sockets.adapter.rooms.get(roomId);

          for (const clientId of clients) {
            const clientSocket = io.sockets.sockets.get(clientId);
            clientSocket.leave(roomId);
          }

          rooms.splice(i, 1);
        }
        break;
      }
      i++;
    }
  });
});

server.listen(port, "0.0.0.0", () => {
  console.log(`Listening on port ${port}`);
});
