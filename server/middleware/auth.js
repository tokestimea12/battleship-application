import jwt from "jsonwebtoken";
import secret from "../utils/config.js";

export function auth(req, res, next) {
  try {
    const token = req.header("token");
    if (!token) return res.status(401).json({ msg: "Access denied!" });

    const verified = jwt.verify(token, secret);
    if (!verified) return res.status(401).json({ msg: "Access denied!" });

    req.userName = verified.userName;
    req.token = token;
    next();
  } catch (e) {
    res.status(500).json({ error: e.message });
  }
}

export function authForSocket(socket, next) {
  try {
    const token = socket.handshake.auth.token;
    if (!token) return next(new Error("Access denied!"));

    const verified = jwt.verify(token, secret);
    if (!verified) return next(new Error("Access denied!"));

    next();
  } catch (e) {
    next(new Error("Internal server error!"));
  }
}
