export async function validateUpload(req, res, next) {
  const dataImg = req.files.newProfilePicture;
  if (!dataImg.type.match("^image/")) {
    return res.status(400).json({ errmsg: "Invalid file format!" });
  }
  return next();
}

export async function validateUploadFleetPicture(req, res, next) {
  const dataImg = req.files.picture;
  if (!dataImg.type.match("^image/")) {
    return res.status(400).json({ errmsg: "Invalid file format!" });
  }
  return next();
}
