class Room {
  roomId;
  player1;
  player2;
  boardSize;
  gameCanStart;
  nrOfUploadedPictures;

  constructor(roomId, player1, boardSize) {
    this.roomId = roomId;
    this.player1 = player1;
    this.player2 = undefined;
    this.boardSize = boardSize;
    this.gameCanStart = 0;
    this.nrOfUploadedPictures = 0;
  }
}

export default Room;
