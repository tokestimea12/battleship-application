import express from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import fs from "fs";
import secret from "../utils/config.js";
import {
  selectUser,
  selectUserIdByName,
  selectUserIdByEmail,
  insertUser,
} from "../db/connection.js";
import { auth } from "../middleware/auth.js";

const router = express.Router();

// login
router.post("/login", async (req, res) => {
  try {
    const { userName, pswd } = req.body;
    const userId = await selectUserIdByName(userName);
    if (userId === -1) {
      return res.status(400).json({ errmsg: "Invalid username or password!" });
    }
    const user = await selectUser(userId);
    if (await bcrypt.compare(pswd, user.userPswd)) {
      const token = jwt.sign({ userName }, secret);
      const picture = fs.readFileSync(
        ".\\static\\" + user.profilePicture,
        "base64"
      );

      return res.json({
        token,
        userName: user.userName,
        email: user.email,
        profilePicture: picture,
      });
    } else {
      return res.status(400).json({ errmsg: "Invalid username or password!" });
    }
  } catch (err) {
    return res.status(500).json({ errmsg: "Internal server error!" });
  }
});

// signup
router.post("/signup", async (req, res) => {
  try {
    const { email, userName, pswd1, pswd2 } = req.body;

    let userId = await selectUserIdByName(userName);
    if (userId !== -1) {
      return res.status(400).json({ errmsg: "Username already exists!" });
    }

    userId = await selectUserIdByEmail(email);
    if (userId !== -1) {
      return res.status(400).json({
        errmsg: "We already have a user registrated with this e-mail!",
      });
    }

    if (pswd1 !== pswd2) {
      return res
        .status(400)
        .json({ errmsg: "The passwords do not match each other!" });
    }

    const userPswd = await bcrypt.hash(pswd1, 10);
    await insertUser({ email, userName, userPswd });
    const token = jwt.sign({ userName }, secret);

    userId = await selectUserIdByEmail(email);
    const user = await selectUser(userId);

    const picture = fs.readFileSync(
      ".\\static\\" + user.profilePicture,
      "base64"
    );

    return res.json({
      token,
      userName: user.userName,
      email: user.email,
      profilePicture: picture,
    });
  } catch (err) {
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.post("/checkToken", async (req, res) => {
  try {
    const token = req.header("token");
    if (!token) return res.json(false);
    const verified = jwt.verify(token, secret);
    if (!verified) return res.json(false);

    const userId = await selectUserIdByName(verified.userName);
    if (userId === -1) {
      return res.json(false);
    }

    const user = await selectUser(userId);
    if (!user) return res.json(false);
    res.json(true);
  } catch (e) {
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.get("/", auth, async (req, res) => {
  try {
    const userId = await selectUserIdByName(req.userName);
    if (userId === -1) {
      return res.json(false);
    }
    const user = await selectUser(userId);
    const picture = fs.readFileSync(
      ".\\static\\" + user.profilePicture,
      "base64"
    );
    res.json({
      token: req.token,
      userName: user.userName,
      email: user.email,
      profilePicture: picture,
    });
  } catch (error) {
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

export default router;
