import express from "express";
import {
  insertGame,
  selectUserIdByName,
  selectWinnerNameByGameId,
  uploadLoserFleetPicture,
  uploadWinnerFleetPicture,
  selectGame,
  selectWinnerFleetPictureByGameId,
  selectLoserFleetPictureByGameId,
  selectUserName,
} from "../db/connection.js";
import { auth } from "../middleware/auth.js";
import { validateUploadFleetPicture } from "../middleware/validators.js";
import eformidable from "express-formidable";
import fs from "fs";

const router = express.Router();

const uploadDir = "static/Images";
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

router.post("/games", auth, async (req, res) => {
  try {
    const {
      winnerUserName,
      loserUserName,
      gameTimeInSeconds,
      numberOfRounds,
      winnerPointsEarned,
      loserPointsEarned,
      gameDate,
    } = req.body;
    let winnerUserId = await selectUserIdByName(winnerUserName);

    if (winnerUserId === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    let loserUserId = await selectUserIdByName(loserUserName);

    if (loserUserId === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    const { insertId } = await insertGame({
      winnerUserId,
      loserUserId,
      gameTimeInSeconds,
      numberOfRounds,
      winnerPointsEarned,
      loserPointsEarned,
      gameDate,
    });
    res.json({ gameId: insertId });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.get("/games/:gameId", auth, async (req, res) => {
  try {
    const gameId = req.params["gameId"];
    const game = await selectGame(gameId);
    if (!game) {
      res.status(400).json({ errmsg: "Inexistent game!" });
    }
    let winnerUserName = await selectUserName(game.winnerUserId);

    if (winnerUserName === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    let loserUserName = await selectUserName(game.loserUserId);

    if (loserUserName === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }
    res.json({ ...game, winnerUserName, loserUserName});
  } catch (error) {
    console.log(error);
    return res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.patch(
  "/games/:gameId/:userName",
  auth,
  eformidable({
    uploadDir,
    keepExtensions: true,
    allowEmptyFiles: false,
  }),
  validateUploadFleetPicture,
  async (req, res) => {
    try {
      const gameId = req.params["gameId"];
      const userName = req.params["userName"];
      const picture = req.files.picture;
      const winnerName = await selectWinnerNameByGameId(gameId);
      if (winnerName === -1) {
        return res.status(400).json({ errmsg: "Inexistent game!" });
      }
      if (userName === winnerName) {
        await uploadWinnerFleetPicture(gameId, picture.path.substring(7));
      } else {
        await uploadLoserFleetPicture(gameId, picture.path.substring(7));
      }
      res.json();
    } catch (error) {
      console.log(error);
      res.status(500).json({ errmsg: "Internal server error!" });
    }
  }
);

router.get("/games/:gameId/pictures", auth, async (req, res) => {
  try {
    const gameId = req.params["gameId"];

    const uploadedPicture1 = await selectWinnerFleetPictureByGameId(gameId);
    const picture1 = fs.readFileSync(
      ".\\static\\" + uploadedPicture1,
      "base64"
    );

    const uploadedPicture2 = await selectLoserFleetPictureByGameId(gameId);
    const picture2 = fs.readFileSync(
      ".\\static\\" + uploadedPicture2,
      "base64"
    );

    res.json({ winnerPicture: picture1, loserPicture: picture2 });
  } catch (error) {
    console.log(error);
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

export default router;
