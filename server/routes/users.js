import express from "express";
import bcrypt from "bcrypt";
import fs from "fs";
import eformidable from "express-formidable";
import { auth } from "../middleware/auth.js";
import {
  updateUserName,
  selectUserIdByName,
  selectPasswordByUser,
  updatePasswordForUser,
  updateProfilePictureForUser,
  selectProfilePictureByUser,
  updatePointsForUser,
  selectWonGamesForUser,
  selectLostGamesForUser,
  selectPointsByUser,
  selectAllGamesForUser,
  selectLeaderboard,
  selectUserName,
} from "../db/connection.js";
import { validateUpload } from "../middleware/validators.js";

const router = express.Router();

const uploadDir = "static/Images";
if (!fs.existsSync(uploadDir)) {
  fs.mkdirSync(uploadDir);
}

router.patch("/users/:userName/changeUserName", auth, async (req, res) => {
  const oldUserName = req.params["userName"];
  const { newUserName } = req.body;
  try {
    let userId = await selectUserIdByName(oldUserName);

    if (userId === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    userId = await selectUserIdByName(newUserName);

    if (userId !== -1) {
      return res
        .status(400)
        .json({ errmsg: "This username is already taken!" });
    }

    await updateUserName(oldUserName, newUserName);

    res.json({ userName: newUserName });
  } catch (error) {
    console.log(error);
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.patch("/users/:userName/changePassword", auth, async (req, res) => {
  const oldUserName = req.params.userName;
  const { oldPassword, newPassword1, newPassword2 } = req.body;

  try {
    const userId = await selectUserIdByName(oldUserName);

    if (userId === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    const oldPasswordFromDb = await selectPasswordByUser(userId);

    if (oldPassword === -1) {
      return res.status(400).json({ errmsg: "This user does not exist!" });
    }

    if (!(await bcrypt.compare(oldPassword, oldPasswordFromDb))) {
      return res
        .status(400)
        .json({ errmsg: "The actual password you specified is not correct!" });
    }

    if (newPassword1 !== newPassword2) {
      return res
        .status(400)
        .json({ errmsg: "The two passwords do not match each other!" });
    }

    if (await bcrypt.compare(newPassword1, oldPasswordFromDb)) {
      return res
        .status(400)
        .json({ errmsg: "New password can't be the same as the old one!" });
    }

    const hashedNewPassword = await bcrypt.hash(newPassword1, 10);
    await updatePasswordForUser(userId, hashedNewPassword);

    res.json({});
  } catch (error) {
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.patch(
  "/users/:userName/changeProfilePicture",
  auth,
  eformidable({
    uploadDir,
    keepExtensions: true,
    allowEmptyFiles: false,
  }),
  validateUpload,
  async (req, res) => {
    const userName = req.params.userName;
    const newProfilePicture = req.files.newProfilePicture;

    try {
      let userId = await selectUserIdByName(userName);

      if (userId === -1) {
        return res
          .status(400)
          .json({ errmsg: "This username does not exist!" });
      }

      await updateProfilePictureForUser(
        userId,
        newProfilePicture.path.substring(7)
      );
      const uploadedPicture = await selectProfilePictureByUser(userId);
      const picture = fs.readFileSync(
        ".\\static\\" + uploadedPicture,
        "base64"
      );
      res.json({ profilePicture: picture });
    } catch (err) {
      console.log(err);
      res.status(500).json({ errmsg: "Internal server error!" });
    }
  }
);

router.get("/users/pictures", auth, async (req, res) => {
  try {
    const { userName1, userName2 } = req.query;

    const userId1 = await selectUserIdByName(userName1);

    if (userId1 === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    const uploadedPicture1 = await selectProfilePictureByUser(userId1);
    const picture1 = fs.readFileSync(
      ".\\static\\" + uploadedPicture1,
      "base64"
    );

    const userId2 = await selectUserIdByName(userName2);

    if (userId2 === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    const uploadedPicture2 = await selectProfilePictureByUser(userId2);
    const picture2 = fs.readFileSync(
      ".\\static\\" + uploadedPicture2,
      "base64"
    );

    res.json({ picture1: picture1, picture2: picture2 });
  } catch (error) {
    console.log(error);
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.patch("/users/:userName/updatePoints", auth, async (req, res) => {
  const userName = req.params["userName"];
  const { points } = req.body;
  try {
    const userId = await selectUserIdByName(userName);

    if (userId === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    await updatePointsForUser(userId, points);

    res.json();
  } catch (error) {
    console.log(error);
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.get("/users/:userName/statistics", auth, async (req, res) => {
  const userName = req.params["userName"];
  try {
    const userId = await selectUserIdByName(userName);

    if (userId === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    const won = await selectWonGamesForUser(userId);
    const lost = await selectLostGamesForUser(userId);
    const points = await selectPointsByUser(userName);

    res.json({ points: points, won: won, lost: lost });
  } catch (error) {
    console.log(error);
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.get("/users/:userName/games", auth, async (req, res) => {
  const userName = req.params["userName"];
  try {
    const userId = await selectUserIdByName(userName);

    if (userId === -1) {
      return res.status(400).json({ errmsg: "This username does not exist!" });
    }

    const games = await selectAllGamesForUser(userId);

    for (let i = 0; i < games.length; i++) {
      games[i].winnerUserName = await selectUserName(games[i].winnerUserId);
      games[i].loserUserName = await selectUserName(games[i].loserUserId);

      games[i].winnerFleetPicture = fs.readFileSync(
        ".\\static\\" + games[i].winnerFleetPicture,
        "base64"
      );
      games[i].loserFleetPicture = fs.readFileSync(
        ".\\static\\" + games[i].loserFleetPicture,
        "base64"
      );
    }

    res.json({ games: games });
  } catch (error) {
    console.log(error);
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

router.get("/users/leaderboard", auth, async (req, res) => {
  try {
    const leaderboard = await selectLeaderboard();

    for (let i = 0; i < leaderboard.length; i++) {
      leaderboard[i].profilePicture = fs.readFileSync(
        ".\\static\\" + leaderboard[i].profilePicture,
        "base64"
      );
    }

    res.json({ leaderboard: leaderboard });
  } catch (error) {
    console.log(error);
    res.status(500).json({ errmsg: "Internal server error!" });
  }
});

export default router;
